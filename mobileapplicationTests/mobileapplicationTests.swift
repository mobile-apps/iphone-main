//
//  mobileapplicationTests.swift
//  mobileapplicationTests
//
//  Created by wtccuser on 8/18/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import UIKit
import XCTest


class mobileapplicationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testURLs() {
        //read http://stackoverflow.com/questions/24045570/swift-read-plist
        if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
               println("dict \(dict)")            }
        }
    }
    
    func testDepartmentJSONClient() {
        // Declare our expectation
        let readyExpectation = expectationWithDescription("ready")
        var passed : Bool = true
        var departments: [Department] = []
        let departmentClient = DepartmentClient(hostName: "http://jbossewsalex-javauniversity.rhcloud.com")
        
        departmentClient.fetchDepartments {
            (departments, error) in
            
            // And fulfill the expectation...
            readyExpectation.fulfill()
            
        }//end of fetchDepartments
        
        waitForExpectationsWithTimeout(20) { error in
            // ...
        }
        // This is an example of a functional test case.
        XCTAssert(passed, "Pass")
    }
    
    func testPodcastJSONClient() {
        // Declare our expectation
        let readyExpectation = expectationWithDescription("ready")
        var passed : Bool = true
        var podcasts: [Podcast] = []
        let podcastClient = PodcastClient(hostName: "http://jbossewsalex-javauniversity.rhcloud.com")
        
        podcastClient.fetchPodcasts {
            (podcasts, error) in
            
            // And fulfill the expectation...
            readyExpectation.fulfill()
        } //end of fetchPodcasts
        
        waitForExpectationsWithTimeout(20) { error in
            // ...
        }
        
        //This is an example of a functional test case.
        XCTAssert(passed, "Pass")
    }
    
    
    func testDrivingDirectionJSONClient() {
        
        //Declare our expectation
        let readyExpectation = expectationWithDescription("ready")
        
        var passed : Bool = true
    
        var steps: [Step] = []
        
        let drivingDirectionClient = DrivingDirectionClient(hostName: "http://jbossewsalex-javauniversity.rhcloud.com")
        
        let geoLocation : GEOLocation = GEOLocation(fromLatitude: 35.79449833333333, toLatitude: 35.79449833333333, fromLongitude: -78.60169833333333, toLongitude: -78.60169833333333)
        
        drivingDirectionClient.findDrivingDirections (geoLocation) {(steps, error) in
            
            readyExpectation.fulfill()
        }
        
        
        //This is an example of a functional test case.
        XCTAssert(passed, "Pass")
        
    } //testDrivingDirectionJSONClient
    
    
    func testPerformanceExample() {
        
        // This is an example of a performance test case.
        self.measureBlock() {
            
            // Put the code you want to measure the time of here.
            
        }
        
    } //testPerformanceExample
    
} //class
