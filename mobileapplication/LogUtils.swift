//
//  LogUtils.swift
//  mobileapplication
//
//  Created by wtccuser on 12/29/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import Foundation
/// <#Description#>
public class LogUtils : NSObject {
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func ENTRY_LOG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) -> Void {
        mlog.debug("ENTRY", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func EXIT_LOG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) -> Void {
        mlog.debug("EXIT", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func DEBUG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        mlog.debug(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func INFO(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        mlog.info(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func WARNING(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        mlog.warning(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func SEVERE(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        mlog.severe(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
}

