//
//  DetailViewController_Donate.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/12/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit

public class DetailViewController_Donate: UIViewController {
    
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    
    
    var detailItem: AnyObject? {
        didSet {
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail: AnyObject = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }
    
}
