//
//  MasterViewController.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/12/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit

public class MasterViewController: UITableViewController {
    /// master menu
    var _menuItems : [MenuItem] = []
    var _errorMessage : String =  String();//initialize
    override public func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        /**
        *  determine if error message should be displayed
        */
        if !_errorMessage.isEmpty {
            Alert.Warning(self, message: _errorMessage)
        }
       // if _errorMessage
      //
    }
    
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
             //Alert.Warning(self, message: "test")
        
        do {
          try  _menuItems = MenuHelper.getMenuItems()
            
       
        } catch Application.ApplicationError.DatatypeConfigurationException(let message) {
            _errorMessage = message;
            LogUtils.SEVERE(displayText: message)
            
        }
         catch  {
           //  LogUtils.SEVERE("unknown error")
            
        }
    }
    //MenuHelper
    
    // MARK: - Segues
    
    override public func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetailDonate" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController_Donate
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    // MARK: - Table View
    
    override public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // menui
        return 1
    }
    
    override public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        LogUtils.ENTRY_LOG()
        return _menuItems.count
    }
    
    override public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        let menuItem = _menuItems[indexPath.row]
        cell.textLabel?.text = menuItem.Title
        cell.accessoryType = .DisclosureIndicator
        cell.imageView!.image = UIImage(named: menuItem.ImageName)
        
        
        
        return cell
    }
    
    override public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuItem = _menuItems[indexPath.row]
        self.performSegueWithIdentifier(menuItem.ShowEvent, sender: self)
        LogUtils.EXIT_LOG()
    }
    
}
