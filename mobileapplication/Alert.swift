//
//  Alert.swift
//  mobileapplication
//
//  Created by wtccuser on 1/10/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import Foundation
import UIKit
public class Alert: NSObject {
    
    class func Warning(delegate:UIViewController, message: String) {
             let alertController = UIAlertController(title: "Application Error", message: message, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        
        delegate.presentViewController(alertController, animated: true, completion: nil)
        
        
        
        
        
    }
    
}
