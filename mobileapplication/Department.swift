//
//  Department.swift
//  mobileapplication
//
//  Created by wtccuser on 8/18/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation
/*

*/
//typealias Location = String
typealias Longitude = String
typealias Latitude = String
typealias Icon = String
typealias Email = String
typealias Name = String
typealias Phone = String
typealias DepartmentID = String
typealias GeneralInformation = String

public class Department {
    class var RootProperty : String {
        get {
            return "departments"
        }
    }
   //define json column names used to parse json object
    class var NameProperty : String {
        get {
            return "name"
        }
    }
    
    class var PhoneProperty : String {
        get {
            return "phone"
        }
    }
    
    class var DepartmentIDProperty : String {
        get {
            return "departmentid"
        }
    }
    
    class var GeneralInformationProperty : String {
        get {
            return "generalInformation"
        }
    }
    
    
    var _location : String!
    var location : String {
        get {
            return _location
        }
        set {
            self._location = newValue
        }
    }
    
    var _longitude : String!
    var longitude : String {
        get {
            return _longitude
        }
        set {
            self._longitude = newValue
        }
    }
    
    var _latitude : String!
    var latitude : String {
        get {
            return _latitude
        }
        set {
            self._latitude = newValue
        }
    }
    
    var _icon : String!
    var icon : String {
        get {
            return _icon
        }
        set {
            self._icon = newValue
        }
    }
    
    var _email : String!
    var email : String {
        get {
            return _email
        }
        set {
            self._email = newValue
        }
    }
    
    var _name : String!
    var name : String {
        get {
            return _name;
        }
        set {
            self._name = newValue
        }
    }
    
    var _phone: String!
    var phone: String {
        get {
            return _phone
        }
        set {
            self._phone  = newValue
        }
    }
    
    var _departmentid : String!
    var departmentid : String {
        get {
            return _departmentid
        }
        set {
            self._departmentid = newValue
        }
    }
    
    var _generalInformation :String!
    var generalInformation : String {
        get {
            return _generalInformation
        }
        set {
            self._generalInformation = newValue
        }
    }
    
    
    //Constructor
    //init(location:Location, longitude:Longitude, latitude:Latitude, icon:Icon, email:Email, name:Name, phone:Phone, departmentid: DepartmentID, generalInformation:GeneralInformation) {
    init(longitude:Longitude, latitude:Latitude, icon:Icon, email:Email, name:Name, phone:Phone, departmentid: DepartmentID, generalInformation:GeneralInformation) {
        self._location = location
        self._longitude = longitude
        self._latitude = latitude
        self._icon = icon
        self._email = email
        self._name = name
        self._phone = phone
        self.departmentid = departmentid
        self._generalInformation = generalInformation
    }
    
}
