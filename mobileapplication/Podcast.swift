//
//  Podcast.swift
//  mobileapplication
//
//  Created by Hope Benziger on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation
/*

*/
typealias Title = String
typealias Description = String
typealias MediaLocation = String
typealias PublishDate = String

/**
Initializer class

- parameter title:         title of podcast
- parameter description:   description of podcast
- parameter mediaLocation: mediaLocation of podcast
- parameter publishDate:   publishDate of podcast

*/
public class Podcast {
    
   ////
    
    class var TitleProperty : String {
        get {
            return "title"
        }
    }
    class var DescriptionProperty : String {
        get {
            return "description"
        }
    }
    class var MediaLocationProperty : String {
        get {
            return "mediaLocation"
        }
    }
    class var PublishDateProperty : String {
        get {
            return "pubDate"
        }
    }

    
    // MARK: Getters and Setters
    var _title :String!
    var title : String {
        get {
            return _title
        }
        set {
            self._title = newValue
        }
    }
    
    var _description :String!
    var description : String {
        get {
            return _description
        }
        set {
            self._description = newValue
        }
    }
    
    var _mediaLocation :String!
    var mediaLocation : String {
        get {
            return _mediaLocation
        }
        set {
            self._mediaLocation = newValue
        }
    }
    
    var _publishDate :String!
    var publishDate : String {
        get {
            return _publishDate
        }
        set {
            self._publishDate = newValue
        }
    }
    
    /**
    Initializer class
    
    - parameter title:         title of podcast
    - parameter description:   description of podcast
    - parameter mediaLocation: mediaLocation of podcast
    - parameter publishDate:   publishDate of podcast
    
    */
    init(title: Title, description: Description, mediaLocation: MediaLocation, publishDate: PublishDate) {
        self._title = title
        self._description = description
        self._mediaLocation = mediaLocation
        self._publishDate = publishDate
    }
    
}