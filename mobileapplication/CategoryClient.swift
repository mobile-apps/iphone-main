//
//  CategoryClient.swift
//  mobileapplication
//
//  Created by Christine LaBeff on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

//return array of categories and error code
typealias FetchCategoryCompletion = ([Category]?, NSError?) -> Void
/// <#Description#>
class CategoryClient : NSObject{
    
    let hostName : String //the is restful service host name
    //todo this should be read from the configuration file
    init(hostName: String) {
        self.hostName = hostName
    }
    
    //define the url address
    var urlAddress : NSURL {
        let urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getAllCategories"
        let url:NSURL = NSURL(string: urlpath)!
        return url
        //    return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getAllCategories")! //todo the path should read from config file
    }
    /*
    Make the restful url call and return array of data
    */
    func fetchData ( completionHandler: FetchCategoryCompletion) {
        var request  = NSURLRequest(URL: urlAddress)
        //alwasys make an async http call
        
        
        
    }//end of fetchdata
    
    func fetchCategories(completionHandler: FetchCategoryCompletion) {
        print("starting fetchCategories")
        var request = NSURLRequest(URL: urlAddress)
        NSURLConnection.sendAsynchronousRequest(
            request, queue: NSOperationQueue.currentQueue()!) {
                (response, data, error) in
                //did we encounter an error
                if let error = error {
                    print("fetchCategories Error\(error)")
                    completionHandler(nil, error)
                    return
                }
                //try to parse json
                var err: NSError?
                let jsonResult: Dictionary! = try?NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! Dictionary<String, AnyObject>
                
                //let results: NSArray = jsonResult["results"] as NSArray?
                // var test = jsonResult["results"]
                let jsonA: AnyObject! = try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                
                let categories = jsonA["categories"]! as! [[String : AnyObject]]
                for category in categories {
                    let id = category[Category.IDProperty]! as! String
                    let name = category[Category.NameProperty]! as! String
                    
                    print("category: \(id) \(name)")
                }
                
                
                
                let json: AnyObject? = try?NSJSONSerialization.JSONObjectWithData(
                    data!,
                    options: NSJSONReadingOptions(rawValue: 0))
                if let err = err {
                    print("fetchCategories Error\(error)")
                    completionHandler(nil, error)
                    return
                }
                //loop thru json object and parse data
                //  let results: NSArray = json["results"]! as NSArray
                
                if let json: AnyObject = json {
                    let categories = json["categories"]
                    
                    if let json = json as? [String: AnyObject] {
                        var myCategories: NSArray = json["categories"] as! NSArray
                        for element in myCategories {
                            // print("\(element) ")
                            
                        }
                        
                        var categories: [Category] = []
                        
                        
                        
                        for (key, item) in json {
                            if let item = item as? [String: String] {
                                /*var department = Department(name: item[Department.NameProperty]!,
                                generalInformation: item[Department.GeneralInformationProperty]!)
                                println("depart \(department)")
                                departments.append(department)*/
                            }
                        }//end of for
                        
                        completionHandler(categories, nil)
                        return
                    }
                    completionHandler(nil,
                        NSError(domain: "category", code: 100, userInfo: nil))
                    return
                } else {
                    completionHandler(nil,
                        NSError(domain: "category", code: 101, userInfo: nil))
                    return
                }
        }
    }
    
    
    
} // end of CategoryClient Class