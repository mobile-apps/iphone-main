//
//  DepartmentClient.swift
//  mobileapplication
//
//  Created by wtccuser on 8/19/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

//return array of departments and error code
typealias FetchDataCompletion = ([Department]?, NSError?) -> Void
//@brief: Use it to write a short description about the method, property, class, file, struct, or enum you’re documenting. No line breaks are allowed.
class DepartmentClient : NSObject {
    let hostName : String //the is restful service host name
    //todo this should be read from the configuration file
    init(hostName: String) {
        self.hostName = hostName
    }
  
    //define the url address
    var urlAddress : NSURL {
        let urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getAllDepartments"
            let url:NSURL = NSURL(string: urlpath)!
            return url
    //    return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getAllDepartments")! //todo the path should read from config file
    }
    /**
    <#Description#>
    
    - parameter completionHandler: <#completionHandler description#>
    */
    func fetchData ( completionHandler: FetchDataCompletion) {
        /// <#Description#>
        var request  = NSURLRequest(URL: urlAddress)
        //alwasys make an async http call

            
      
    }//end of fetchdata
    /**
    * Invoke the JSOn web service to return a list of departments
    *
    * @return No return value
    */
    func fetchDepartments(completionHandler: FetchDataCompletion) {
        print("starting fetchDepartments", terminator: "")
        let request = NSURLRequest(URL: urlAddress)
        NSURLConnection.sendAsynchronousRequest(
            request, queue: NSOperationQueue.currentQueue()!) {
                (response, data, error) in
           
                
                //try to parse json
                var err: NSError?
                /*
                let jsonResult: Dictionary? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! Dictionary<String, AnyObject>
                */
               
                //let results: NSArray = jsonResult["results"] as NSArray?
                // var test = jsonResult["results"]
                let jsonA: AnyObject! = try? NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                //did we encounter an error
                if let error = error {
                    print("fetchDepartments Error\(error)")
                    completionHandler(nil, error)
                    return
                }
                
                 let departments = jsonA[Department.RootProperty]! as! [[String : AnyObject]] //list of departments from json
                var departs: [Department] = [] //return departments
                for department in departments {
                    let nameJSON = department[Department.NameProperty]! as! String
                    let generalInformationJSON = department[Department.GeneralInformationProperty]!as! String
                      let phoneJSON = department[Department.GeneralInformationProperty]!as! String
                    //create department object
                    // init(name: Name, generalInformation: GeneralInformation, phone: Phone)
                   //@todo var departObj = Department(name: nameJSON, generalInformation: generalInformationJSON, phone: phoneJSON)
                    //@todo departs.append(departObj)
                    //println("employee: \(name) \(lastName)")
                    }
                

                /*
                
                let json: AnyObject? = NSJSONSerialization.JSONObjectWithData(
                    data,
                    options: NSJSONReadingOptions(0),
                    error: &err)
                if let err = err {
                    println("fetchDepartments Error\(error)")
                    completionHandler(nil, error)
                    return
                }
*/
                //loop thru json object and parse data
                //  let results: NSArray = json["results"]! as NSArray
                print("ending fetchDepartments")
                
        }
    }

    

}//end of class