//
//  TableViewController_UpcomingEvents.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/12/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit

class TableViewController_UpcomingEvents: UITableViewController {
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        // Set appropriate labels for the cells.
        if indexPath.row == 0 {
            cell.textLabel?.text = "Detail View Controller UpcomingEvents"
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            self.performSegueWithIdentifier("showDetail1fromTableView_UpcomingEvents", sender: self)
        }
        
    }
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showDetail1fromTableView_UpcomingEvents" {
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController_UpcomingEvents
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
            controller.view.backgroundColor = UIColor.greenColor()
            
        }
        
        
    }
    
    
}
