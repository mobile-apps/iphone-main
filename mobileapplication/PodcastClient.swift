//
//  PodcastClient.swift
//  mobileapplication
//
//  Created by Hope Benziger on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

///  Array of podcasts and error code
typealias FetchPodcastCompletion = ([Podcast]?, NSError?) -> Void


/// Used to communicate with getAllPodcastForMobileApps RESTful web service

class PodcastClient : NSObject {
   
    
    /// The hostname of the restful web service
    let hostName : String
    
    // TODO: hostName should be read from the configuration file
    init(hostName: String) {
        self.hostName = hostName
    }
    
    /// The url address of the web service
    var urlAddress : NSURL {
        let urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getAllPodcastForMobileApps"
        let url:NSURL = NSURL(string: urlpath)!
        return url
        //    return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getAllDepartments")! //todo the path should read from config file
    }
    
    /**
    Fetches Podcasts
    
    - parameter completionHandler: contains an array of podcast objects and an error string
    */
    func fetchPodcasts(completionHandler: FetchPodcastCompletion) {
       // print("starting fetchPodcasts")
       // let mLog = XCGLogger.defaultInstance()
      // mLog("starting fetchPodcasts")
      LogUtils.ENTRY_LOG()
       // LogUtils.aaa()
        let urlPath = URLHelper.getURLAddress(URLAddress.GetAllPodcasts)
        print(urlPath)
        let url: NSURL = NSURL(string: urlPath)!
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            /**
            *  If there is an error in the web request, print it to the console
            */
            if error != nil {
                print(error!.localizedDescription)
                completionHandler(nil, error)
            }
            
            
            do {
                // Try parsing some valid JSON
                let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                print(jsonResult)
                
                let json = JSON(jsonResult)
                let count: Int? = json["podcast"].array?.count
                print("found \(count!) podcasts")
                
                
                var podcasts = [Podcast]()
                /// Check if any podcasts were returned
                if let x = count {
                    /**
                    *  If podcasts were returned, then loop through and print the Title of each podcast
                    */
                    for index in 0...x-1 {
                        if let title = json["podcast"][index][Podcast.TitleProperty].string {
                            print(title)
                            let description = json["podcast"][index][Podcast.DescriptionProperty].string
                            let mediaLocation = json["podcast"][index][Podcast.MediaLocationProperty].string
                            let publishDate = json["podcast"][index][Podcast.PublishDateProperty].string
                            
                            let podcast = Podcast(title: title, description: description!, mediaLocation: mediaLocation!, publishDate: publishDate!)
                            podcasts.append(podcast)
                        }
                    }
                }
                
                completionHandler(podcasts, nil)
                
                
            }
            catch let error as NSError? {
                print("A JSON parsing error occurred, here are the details:\n \(error)")
                completionHandler(nil, nil)
            }
            
        })
        task.resume()
        
    }
    
} // end of PodcastClient Class