//
//  DrivingDirectionClient.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation


//return directions and error code
typealias findDrivingDirectionsCompletion = ([Step]?, NSError?) -> Void

class DrivingDirectionClient : NSObject {
    
    let hostName : String //the is restful service host name
    
    //todo this should be read from the configuration file
    init(hostName: String) {
        
        self.hostName = hostName
        
    }
    
    
    //Define the url address
    var urlAddress : NSURL {
        
        let urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getGoogleMap"
        
        let url:NSURL = NSURL(string: urlpath)!
        
        return url
        
        //todo the path should read from config file
        //return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getGoogleMap")!
        
    }
    
    
    func fetchData(completionHandler: findDrivingDirectionsCompletion) {
        
        var request  = NSURLRequest(URL: urlAddress)
        
        // always make an async http call
        
    } //fetchdata
    
    
    /**
    * Invoke the JSOn web service to return directions
    *
    * @return No return value
    */
    func findDrivingDirections(geoLocation: GEOLocation, completionHandler: findDrivingDirectionsCompletion) {
        
        //globalLog.info("starting findDrivingDirections")
        
        var fullURL : String
        fullURL = String(format:"%f", urlAddress)
        fullURL += "?latitudeStart=" + String(format:"%f", geoLocation.fromLatitude)
        fullURL += "&longitudeStart=" + String(format:"%f", geoLocation.fromLongitude)
        fullURL += "&latitudeEnd=" + String(format:"%f", geoLocation.toLatitude)
        fullURL += "&longitudeEnd=" + String(format:"%f", geoLocation.toLongitude)
        
       // globalLog.debug("fullURL : " + fullURL)
        
        
        let request = NSURLRequest(URL: NSURL(string: fullURL)!)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.currentQueue()!) {
            
            (response, data, error) in
            
            //try to parse json
            var err: NSError?
            
            let jsonA: AnyObject! = try? NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers)
            
            //If error
            if let error = error {
                
               // globalLog.error("findDrivingDirections Error: " + error.localizedDescription)
                
                completionHandler(nil, error)
                
                return
                
            } //if let error = error
            
            
            var returnSteps : [Step] = [] //return Steps
            
            let startingAddress = jsonA[Direction.startingAddressProperty]! as! [[String : AnyObject]]
            let endingAddress = jsonA[Direction.endingAddressProperty]! as! [[String:AnyObject]]
            let steps = jsonA[Direction.stepsProperty]! as! [[String:AnyObject]]
            
            for step in steps {
                
                let distanceJSON = step[Step.distanceProperty]! as! String
                let durationJSON = step[Step.durationProperty]! as! String
                let htmlInstructionJSON = step[Step.htmlInstructionProperty]! as! String
                let manueverJSON = step[Step.manueverProperty]! as! String
                
                let stepObject = Step(distance: distanceJSON, duration: durationJSON, manuever: manueverJSON, htmlInstruction: htmlInstructionJSON)
                
                returnSteps.append(stepObject)
            
            } //for step in steps
                        
            
          //  globalLog.info("ending findDrivingDirections")
            
        } //sendAsynchronousRequest
        
    } //findDrivingDirections
    
    
} //class
