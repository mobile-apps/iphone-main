//
//  DetailViewController_Podcast.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/13/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit

class DetailViewController_Podcast: UIViewController {
    
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    
    
    var detailItem: AnyObject? {
        didSet {
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail: AnyObject = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }
    
    
    
}

