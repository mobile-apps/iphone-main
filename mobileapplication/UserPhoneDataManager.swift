//
//  UserPhoneDataManager.swift
//  mobileapplication
//
//  Created by wtccuser on 9/23/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

public enum UserKey : String
{
    case Registration = "registrationId"
    
}

/// Save and retreive phone local data
class UserPhoneDataManager {
    /**
    <#Description#>
    
    - parameter userKey:   key to store information
    - parameter dataValue: bool value to store with key
    */
    class func setValue(userKey : UserKey, dataValue : Bool ) {
 /// get the user default values
        let defaults = NSUserDefaults.standardUserDefaults()
        /**
        set the bool value
        
        - parameter forKey: <#forKey description#>
        */
        defaults.setBool(dataValue, forKey: userKey.rawValue)
        
        defaults.synchronize()
    }
    /**
    <#Description#>
    
    - parameter userKey:   <#userKey description#>
    - parameter dataValue: <#dataValue description#>
    */
    class func setValue(userKey : UserKey, dataValue : String ) {
        /// get the user default values
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(dataValue, forKey: userKey.rawValue)
       // defaults.(dataValue, forKey: userKey.rawValue)
        
        defaults.synchronize()
    }
    /**
    <#Description#>
    
    - parameter userKey:   <#userKey description#>
    - parameter dataValue: <#dataValue description#>
    */
    class func setValue(userKey : UserKey, dataValue : Int ) {
        /// get the user default values
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(dataValue, forKey: userKey.rawValue)
        // defaults.(dataValue, forKey: userKey.rawValue)
        
        defaults.synchronize()
    }
    /**
    <#Description#>
    
    - parameter userkey: <#userkey description#>
    
    - returns: return bool value of key
    */
    class func getBoolValue(userkey : UserKey) -> Bool {
        // get the user default values
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let boolValue = defaults.valueForKey(userkey.rawValue) as? Bool {
            return boolValue
        }
        return false
        
   }
    /**
    <#Description#>
    
    - parameter userkey: <#userkey description#>
    
    - returns: <#return value description#>
    */
    class func getStringValue(userkey : UserKey) -> String {
        // get the user default values
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let stringValue = defaults.valueForKey(userkey.rawValue) as? String {
            return stringValue
        }
        return String()
        
    }
    /**
    <#Description#>
    
    - parameter userkey: <#userkey description#>
    
    - returns: <#return value description#>
    */
    class func getIntValue(userkey : UserKey) -> Int  {
        // get the user default values
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let intValue = defaults.valueForKey(userkey.rawValue) as? Int {
           return intValue
        }
        return 0
        
    }
    /**
    <#Description#>
    
    - returns: <#return value description#>
    */
    class func getRegistrationId() -> String
    {
       let registrationId = getStringValue(UserKey.Registration)
        return registrationId
    }
    /**
    <#Description#>
    
    - returns: <#return value description#>
    */
    class func hasRegistered() -> Bool {
        let registrationId = getStringValue(UserKey.Registration)
        /**
        *  <#Description#>
        *
        *  @param registrationId <#registrationId description#>
        *
        *  @return <#return value description#>
        */
        if (registrationId == String()) {
           return false
        }
        return true
        
    }
    
}
