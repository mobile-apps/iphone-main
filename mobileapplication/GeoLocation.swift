//
//  GeoLocation.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 9/1/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation


public class GEOLocation
{
    var _fromLatitude : Double!
    var fromLatitude : Double {
        get {
            return _fromLatitude
        }
        set {
            _fromLatitude = newValue
        }
    }
    

    var _toLatitude : Double!
    var toLatitude : Double {
        get {
            return _toLatitude
        }
        set {
            _toLatitude = newValue
        }
    }

    
    var _fromLongitude : Double!
    var fromLongitude : Double {
        get {
            return _fromLongitude
        }
        set {
            _fromLongitude = newValue
        }
    }
    
    
    var _toLongitude : Double!
    var toLongitude : Double {
        get {
            return _toLongitude
        }
        set {
            _toLongitude = newValue
        }
    }
  
    
    //Constructor
    init(fromLatitude : Double, toLatitude : Double, fromLongitude : Double, toLongitude : Double) {
        
        _fromLatitude = fromLatitude
        _toLatitude = toLatitude
        _fromLongitude = fromLongitude
        _toLongitude = toLongitude
        
    }
    
} //class
