///
//  AppDelegate.swift
//  mobileapplication
//
//  Created by wtccuser on 8/18/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import UIKit

//todo global variable XCGLogger
//let ddd  = XCGLogger.defaultInstance()
public let mlog = XCGLogger.defaultInstance()

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        /**
        *  asyn task
        */
        //Async.background {
         //   print("background")
          //  CrashReport.log(displayText: "test")
       // }
        //initialize logger
        // log.setup(logLevel: .Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: "path/to/file", fileLogLevel: .Debug)
        
        mlog.info("starting application")
        
        if let path = NSBundle.mainBundle().pathForResource("application", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                let loggingLevel : String = dict["loggingLevel"]!
                switch loggingLevel.lowercaseString
                {
                case "debug":
                    mlog.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Debug)
                case "info":
                    mlog.setup(.Info, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Info)
                case "warning":
                    mlog.setup(.Warning, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Warning)
                case "error":
                    mlog.setup(.Error, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Error)
                case "severe":
                    mlog.setup(.Severe, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Severe)
                default:
                    mlog.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true,  fileLogLevel: .Debug)
                }
                
                
            }
        }
        //
        // Override point for customization after application launch.
        let splitViewController = self.window!.rootViewController as! UISplitViewController
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
        splitViewController.delegate = self
        return true
    }
    
    // MARK: - Split view
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
        if let secondaryAsNavController = secondaryViewController as? UINavigationController {
            if let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController_Donate {
                if topAsDetailController.detailItem == nil {
                    // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
                    //If we don't do this, detail1 will open as the first view when run on iPhone, comment and see
                    return true
                }
            }
        }
        return false
    }

    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

