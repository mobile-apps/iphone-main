//
//  Organization.swift
//  mobileapplication
//
//  Created by Shanthan Citineni on 8/30/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

typealias Location = String
typealias Latitude = String
typealias Longitude = String
typealias Image = String
typealias Email = String
typealias Name = String
typealias Phone = String
typealias GeneralInformation = String

public class Organization {
    
    class var LocationProperty : String {
        get {
            return "location"
        }
    }
    
    class var LatitudeProperty : String {
        get {
            return "latitude"
        }
    }
    
    class var LongitudeProperty : String {
        get {
            return "longitude"
        }
    }
    
    class var ImageProperty : String {
        get {
            return "image"
        }
    }
    
    class var EmailProperty : String {
        get {
            return "email"
        }
    }
    
    class var NameProperty : String {
        get {
            return "name"
        }
    }
    
    class var PhoneProperty : String {
        get {
            return "phone"
        }
    }
    
    class var GeneralInformationProperty : String {
        get {
            return "generalinformation"
        }
    }
    
    var _id : Int!
    var id : Int {
        get {
            return _id
        }
        set {
            self._id = newValue
        }
    }
    
    var _location : String!
    var location : String {
        get {
            return _location
        }
        set {
            self._location = newValue
        }
    }
    
    var _latitude : String!
    var latitude : String {
        get {
            return _latitude
        }
        set {
            self._latitude = newValue
        }
    }
    
    var _longitude : String!
    var longitude : String {
        get {
            return _longitude
        }
        set {
            self._longitude = newValue
        }
    }
    
    var _image : String!
    var image : String {
        get {
            return _image
        }
        set {
            self._image = newValue
        }
    }
    
    var _email : String!
    var email : String {
        get {
            return _email
        }
        set {
            self._email = newValue
        }
    }
    
    var _name : String!
    var name : String {
        get {
            return _name
        }
        set {
            self._name = newValue
        }
    }
    
    var _phone : String!
    var phone : String {
        get {
            return _phone
        }
        set {
            self._phone = newValue
        }
    }
    
    var _generalInformation : String!
    var generalInformation : String {
        get {
            return _generalInformation
        }
        set {
            self._generalInformation = newValue
        }
    }
    
    var _list : [Department]!
    var list : [Department] {
        get {
            return _list
        }
        set {
            self._list = newValue
        }
    }
    
    init(email:Email, generalInformation:GeneralInformation, location:Location, latitude:Latitude, longitude:Longitude, image:Image, name:Name, phone:Phone) {
        self._email = email;
        self._generalInformation = generalInformation;
        self._location = location;
        self._latitude = latitude;
        self._longitude = longitude;
        self._image = image;
        self._name = name;
        self._phone = phone;
    }
    
}
