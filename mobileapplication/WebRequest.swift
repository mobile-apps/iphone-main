//
//  WebRequest.swift
//  mobileapplication
//
//  Created by wtccuser on 8/20/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation
enum RequestType
{
    case Post
    case Get
    
}
class WebRequest {
    
   var _urlAddress : String!
    var _requestType : RequestType!
    
    var requestType : RequestType {
        get {
           return  _requestType
        }
        set {
            _requestType = newValue
        }
    }
    var urlAddress : String {
        get {
            return _urlAddress
        }
        set {
            _urlAddress = newValue
        }
    }
    
    
}
