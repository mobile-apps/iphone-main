//
//  GPSLocation.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

//typealias Latitude = Double
//typealias Longitude = Double
typealias IsLocationAvailable = Bool
typealias IsNetworkAvailable = Bool

/// <#Description#>
public class GPSLocation
{
    
    /*
    var _latitude : Double!
    var latitude : Double {
        get {
            return _latitude
        }
        set {
            self._latitude = newValue
        }
    }

    
    var _longitude : Double!
    var longitude : Double {
        get {
            return _longitude
        }
        set {
            self._longitude = newValue
        }
    }
    */
    
    
    var _isLocationAvailable : Bool!
    var isLocationAvailable : Bool {
        get {
            return _isLocationAvailable
        }
        set {
            self._isLocationAvailable = newValue
        }
    }

    
    var _isNetWorkAvailable : Bool!
    var isNetWorkAvailable : Bool {
        get {
            return _isNetWorkAvailable
        }
        set {
            self._isNetWorkAvailable = newValue
        }
    }
    
    
    //Constructor
    /*
    init (latitude : Latitude, longitude : Longitude) {
        
        self._latitude = latitude
        self._longitude = longitude
        
    }
    */
    
    /**
    <#Description#>
    
    - parameter isNetworkAvailable:  <#isNetworkAvailable description#>
    - parameter isLocationAvailable: <#isLocationAvailable description#>
    
    - returns: <#return value description#>
    */
    init (isNetworkAvailable : IsNetworkAvailable, isLocationAvailable : IsLocationAvailable) {
        
        self._isNetWorkAvailable = isNetworkAvailable
        self._isLocationAvailable = isLocationAvailable
        
    }
    
} // class