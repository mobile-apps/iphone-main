//
//  iosappTests.swift
//  iosappTests
//
//  Created by wtccuser on 1/22/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import XCTest
@testable import iosapp


class iosappTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    func testDepartment() {
        //This is an example of a functional test case.
        let readyExpectation = expectationWithDescription("ready")
        let departments: [Department] = []
       
        let client = DepartmentClient()
        client.fetchDepartments {
            (departments, error) in
            
            // And fulfill the expectation...
            readyExpectation.fulfill()
        } //end of fetchPodcasts
        
        waitForExpectationsWithTimeout(20) { error in
            // ...
        }
       
        
        XCTAssert(true, "Pass")
    }

    func testAllEvents() {
        // Declare our expectation
        let readyExpectation = expectationWithDescription("ready")
        let events: [Event] = []
        let eventRestfulClient = EventRestfulClient()
        eventRestfulClient.fetchEvents {
            (events, error) in
            
            // And fulfill the expectation...
            readyExpectation.fulfill()
        } //end of fetchPodcasts
        
        waitForExpectationsWithTimeout(20) { error in
            // ...
        }
        
        
        //This is an example of a functional test case.
        XCTAssert(true, "Pass")
        
    }
    func testPodcastJSONClient() {
        // Declare our expectation
        let readyExpectation = expectationWithDescription("ready")
       
        var podcasts: [Podcast] = []
        let podcastClient = PodcastClient(hostName: "http://jbossewsalex-javauniversity.rhcloud.com")
        
        podcastClient.fetchPodcasts {
            (podcasts, error) in
            
            // And fulfill the expectation...
            readyExpectation.fulfill()
        } //end of fetchPodcasts
        
        waitForExpectationsWithTimeout(20) { error in
            // ...
        }
        

        //This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    
}
