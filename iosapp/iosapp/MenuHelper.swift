//
//  MenuHelper.swift
//  mobileapplication
//
//  Created by wtccuser on 10/23/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import Foundation
//// Class used to construct menu
public class MenuHelper {
      //  public class func sortMenu
    /**
    Read the menu plist  file to construct menu items
    
    - parameter urlAddress: <#urlAddress description#>
    
    - returns: <#return value description#>
    */
    public class func getMenuItems() throws -> [MenuItem] {
        
        LogUtils.ENTRY_LOG()
        var menuItems : [MenuItem] = []
        if let path = NSBundle.mainBundle().pathForResource("menu", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> {
                // use swift dictionary as normal
                //loop thru the dictionary
                for name in dict.values {
                    //LogUtils.DEBUG(name)
                    // print(name)
                    let dictArray = name as! NSArray
                    //  print(dictArray)
                    let title = dictArray[0]
                    let imageName = dictArray[1]
                   // let position = dictArray[2]
                    
                    guard let position = dictArray[2] as? Int  else {
                        let text = NSLocalizedString("menu parsing error", comment: "Error parsing position to integer")
                        LogUtils.SEVERE(displayText: "error parsing position ");
                        throw Application.ApplicationError.ParseException(message: text)
                        
                    }
                    
                    
                    
                    let showEvent = dictArray[3]
                    
                    
                    let menuItem = MenuItem()
                    menuItem.Title = title as! String
                    menuItem.Position = position
                    menuItem.ImageName = imageName as! String
                    menuItem.ShowEvent = showEvent as! String
                    menuItems.append(menuItem)
                    
                }
                
                LogUtils.DEBUG(displayText:dict.description)
                //print(dict)
            }
        }
       // menuItems.
        menuItems.sortInPlace { (element1, element2) -> Bool in
            return element1.Position < element2.Position
        }
        LogUtils.EXIT_LOG()
        return menuItems
    }
    
}
