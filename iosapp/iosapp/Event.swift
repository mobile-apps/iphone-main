//
//  Event.swift
//  mobileapplication
//
//  Created by David E Weiss on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

typealias StartDate = NSDate
typealias EndDate = NSDate
typealias Location = String
//typealias Name = String
//typealias Phone = String
//typealias GeneralInformation = String
typealias DateFormatted = String

public class Event {
    
    // Define JSON properties
    class var StartDateProperty : String {
        get {
            return "startDateLong"
        }
    }
    class var EndDateProperty : String {
        get {
            return "endDateLong"
        }
    }
    class var DepartmentProperty : String {
        get {
            return "department"
        }
    }
    class var LocationProperty : String {
        get {
            return "location"
        }
    }
    class var NameProperty : String {
        get {
            return "name"
        }
    }
    class var PhoneProperty : String {
        get {
            return "phone"
        }
    }
    class var GeneralInformationProperty : String {
        get {
            return "generalInformation"
        }
    }
    
    // Define class variables
    var _startDate : StartDate!
    var startDate : StartDate {
        get {
            return _startDate;
        }
        set {
            self._startDate = newValue
        }
    }
    
    var _endDate : EndDate!
    var endDate : EndDate {
        get {
            return _endDate;
        }
        set {
            self._endDate = newValue
        }
    }
    
    var _department : Department!
    var department : Department {
        get {
            return _department;
        }
        set {
            self._department = newValue
        }
    }
    
    var _location : Location!
    var location : Location {
        get {
            return _location;
        }
        set {
            self._location = newValue
        }
    }
    
    var _name : Name!
    var name : Name {
        get {
            return _name;
        }
        set {
            self._name = newValue
        }
    }
    
    var _phone : Phone!
    var phone : Phone {
        get {
            return _phone;
        }
        set {
            self._phone = newValue
        }
    }
    
    var _generalInformation : GeneralInformation!
    var generalInformation : GeneralInformation {
        get {
            return _generalInformation;
        }
        set {
            self._generalInformation = newValue
        }
    }
    /// Compute formatted date
    var formatedStartDate : String {
        get {
            let formatter = NSDateFormatter()
            //formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            //formatter.timeStyle = .ShortStyle
            formatter.dateFormat = "EEE, h a, MMM yy"
            
            let dateString = formatter.stringFromDate(_startDate)
            
            
            return dateString
        }
    }
    /// Compute formatted date
    var formatedEndDate : String {
        get {
            let formatter = NSDateFormatter()
            //formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            //formatter.timeStyle = .ShortStyle
            formatter.dateFormat = "EEE, h a, MMM yy"
            
            let dateString = formatter.stringFromDate(_endDate)
            
            
            return dateString
        }
    }
    
    //func FormatedDate()  {
    //    return DateHelper.getDefaultFormatDate(_startDate)
    //}
    
    // Define constructors
    
    init(startDate : StartDate, endDate : EndDate, department : Department, location : Location, name : Name, generalInformation : GeneralInformation, phone:Phone) {
        self._startDate = startDate
        self._endDate = endDate
        self._department = department
        self._location = location
        self._name = name
        self._generalInformation = generalInformation
        self._phone = phone
    }
    
    init(startDate : StartDate, endDate : EndDate, name : Name, generalInformation : GeneralInformation, phone:Phone) {
        self._startDate = startDate
        self._endDate = endDate
        self._name = name
        self._generalInformation = generalInformation
        self._phone = phone
    }
}
