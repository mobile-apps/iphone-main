	import UIKit
//import YouTubePlayer

//import ImageSlideshow
//class FirstViewController: TabVCTemplate, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate 
class FirstViewController: BaseTabUIViewController {
    let gradientLayer = CAGradientLayer()
  
   
   
    @IBOutlet weak var youTubePlayerView: YouTubePlayerView!
    @IBOutlet weak var bottomUILabel: UILabel!
    @IBOutlet weak var middleUILabel: UILabel!
    @IBOutlet weak var slideUIScrollView: UIScrollView!
    @IBOutlet weak var headerUILabel: UILabel!
        var delegate:AppDelegate?
    let customNavigationAnimationController = CustomNavigationAnimationController()
    
    
   // var delegate:AppDelegate?
    /**
     <#Description#>
     */
    func goBack() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        
        
        let menuViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! LeftMenu
       
        let navigationController = self.delegate!.window!.rootViewController as! UINavigationController
      
        //let customNavigationAnimationController = CustomNavigationAnimationController()
        
        navigationController.presentViewController(menuViewController, animated: true, completion: nil)
        
        //let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        //navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        customNavigationAnimationController.reverse = operation == .Pop
        return customNavigationAnimationController
    }
    
    func loadYouTube() {
       youTubePlayerView.playerVars = [
            "playsinline": "1",
            "controls": "0",
            "showinfo": "0"
        ]
        
    
        youTubePlayerView.loadVideoID("nZ-OPZpW2lk")
    }
    /**
     <#Description#>
     
     - parameter sender: <#sender description#>
     */

    
    /**
     load
     */
    override func viewDidLoad() {
        super.viewDidLoad()
         LogUtils.INFO(displayText: "starting viewDidLoad")
        //youTubePlayer.backgroundColor = .redColor()
        //loadYouTube()
        //log youtube
        
        //var testyouTubePlayer = YouTubePlayerView(frame: self.view.frame)
   
        
        // init YouTubePlayerView w/ playerFrame rect (assume playerFrame declared)
       
         headerUILabel.font = UIFont.boldSystemFontOfSize(14.0)
          middleUILabel.font = UIFont.boldSystemFontOfSize(17.0)
          bottomUILabel.font = UIFont.boldSystemFontOfSize(10.0)
        let backgroundImage = UIImageView(frame: UIScreen.mainScreen().bounds)
        backgroundImage.image = UIImage(named: "background")
        self.view.insertSubview(backgroundImage, atIndex: 0)
        
       
        // slide.auk.show(url: "http://revamp-revamp.rhcloud.com/faces/javax.faces.resource/bible-precepts.png?ln=images")
        // Show local image
       // addSlideMenuButton()
     //    navigationController?.delegate = self        // var images
       // var images = [UIImage]()
      
       // let leftButton =  UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DetailViewController_UpcomingEvents.goBack))
        //let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.Plain, target: self, action: nil)
    self.navigationController?.navigationBarHidden = true
       // navigationItem.leftBarButtonItem = leftButton
        
        LogUtils.INFO(displayText: "load image")
        
      
        //let screenSize: CGRect = UIScreen.mainScreen().bounds
        // var logoImage = UIImage(named: "logo")
       // slide.frame = CGRectMake(0,0, screenSize.height * 0.2, 50)

        
        //slideUIScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)
        //slide.contentSize = CGSizeMake(360, 400)
    
        // Scroll images automatically with the interval of 3 seconds

       // self.view.backgroundColor = UIColor.greenColor()
        
        // 2
       gradientLayer.frame = self.view.bounds
        
        // 3
        
       //let color2 = UIColor(red: 0.4118, green: 0.1686, blue: 0.5647, alpha: 1.0) /* #692b90 */
       // let color2 = UIColor(red:0.28, green:0.08, blue:0.25, alpha:1.0)
        //let color2 = UIColor(red:0.28, green:0.08, blue:0.25, alpha:1.0).CGColor as CGColorRef
      //  let color4 = UIColor(red:0.85, green:0.35, blue:0.79, alpha:1.0).CGColor as CGColorRef
        //gradientLayer.colors = [color2, color2]
        UINavigationBar.appearance().barTintColor = UIColor(red: 46.0/255.0, green: 14.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
    
        //let layer = CAGradientLayer()
        //layer.frame = self.view.bounds
       // layer.colors = [color2, color4]
        //self.view.layer.addSublayer(layer)
       // self.view.layer.insertSublayer(layer, atIndex: 0)
    //self.view.layer.insertSublayer(gradientLayerView.layer, atIndex: 0)
        LogUtils.INFO(displayText: "ending viewDidLoad")        
    }
    
    @IBOutlet weak var slide: UIScrollView!
    @IBOutlet var mainUIView: UIView!
    @IBAction func toggleMenu(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("toggleMenu", object: nil)
    }
    
}


