//
//  PodcastClient.swift
//  mobileapplication
//
//  Created by Hope Benziger on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

///  Array of podcasts and error code
typealias FetchEventCompletion = ([Event]?, NSError?) -> Void


/// Used to communicate with getAllPodcastForMobileApps RESTful web service

class EventRestfulClient : NSObject {
    /**
     Fetches Podcasts
     
     - parameter completionHandler: contains an array of podcast objects and an error string
     */
    func fetchEvents(completionHandler: FetchEventCompletion) {
        // print("starting fetchPodcasts")
        // let mLog = XCGLogger.defaultInstance()
        // mLog("starting fetchPodcasts")
        LogUtils.ENTRY_LOG()
        // LogUtils.aaa()
        let urlPath = URLHelper.getURLAddress(URLAddress.GetAllEvents)
        print(urlPath)
        let url: NSURL = NSURL(string: urlPath)!
      //  let session = NSURLSession.sharedSession()
        let urlconfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlconfig.timeoutIntervalForRequest = 30
        urlconfig.timeoutIntervalForResource = 30
       
           let session = NSURLSession.init(configuration:urlconfig)
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            /**
            *  If there is an error in the web request, print it to the console
            */
            if error != nil {
                LogUtils.SEVERE(displayText: error!.localizedDescription)
               // print(error!.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            
            do {
                // Try parsing some valid JSON
                let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                //print(jsonResult)
                
                let json = JSON(jsonResult)
                
                let count: Int? = json.array?.count
                LogUtils.DEBUG(displayText: "found \(count!) events")
               
                
                
                var events = [Event]()
                /// Check if any events were returned
                if let x = count {
                    /**
                    *  If podcasts were returned, then loop through and print the Title of each podcast
                    */
                    for index in 0...x-1 {
                        if let name = json[index][Event.NameProperty].string {
                            //print(name)
                            let phone = json[index][Event.PhoneProperty].string
                            let generalInformation = json[index][Event.GeneralInformationProperty].string
                            var startDate = json[index][Event.StartDateProperty].double
                            startDate = startDate! / 1000.0
                            let startNSDatendate = NSDate(timeIntervalSince1970: startDate!)
                            var endDate = json[index][Event.EndDateProperty].double
                            endDate = endDate! / 1000.0
                            let endNSDatendate = NSDate(timeIntervalSince1970: endDate!)
                            let department = json[index][Event.DepartmentProperty].dictionaryObject
                            //print(department)
                           let departmentName = department![Department.NameProperty] as! String
                           let departmentLocation = department![Department.LocationProperty] as! String
                           let departmentLatitude = department![Department.LatitudeProperty] as! Double
                            let departmentLongitude = department![Department.LongitudeProperty] as! Double
                            
                            //let d = [NSDate dateWithTimeIntervalSince1970 : 1379598284];
                            let departmentEmail = department![Department.EmailProperty] as! String
                            let departmentIcon = department![Department.IconProperty] as! String
                             let departmentPhone = department![Department.PhoneProperty] as! String
                            let departmentGeneralInformation = department![Department.GeneralInformationProperty] as! String
                            
                            let departmentId = department![Department.DepartmentIDProperty] as! Int
                        
                            
                            let departmentObj = Department(location: departmentLocation,longitude: departmentLongitude, latitude: departmentLatitude, icon: departmentIcon, email: departmentEmail, name: departmentName, phone: departmentPhone, departmentid: departmentId, generalInformation: departmentGeneralInformation)
                            
                            let eventObj = Event(startDate: startNSDatendate, endDate: endNSDatendate, department: departmentObj, location: departmentLocation, name: name, generalInformation: generalInformation!, phone: phone!)
                            //let podcast = Podcast(title: title, description: description!, mediaLocation: mediaLocation!, publishDate: publishDate!)
                           events.append(eventObj)
                        }
                    }
                }
                
                completionHandler(events, nil)
                
                
            }
            catch let error as NSError? {
                LogUtils.SEVERE(displayText: "A JSON parsing error occurred, here are the details:\n \(error)")
                
                completionHandler(nil, nil)
            }
            
        })
        task.resume()
        
    }
    
} // end of PodcastClient Class
