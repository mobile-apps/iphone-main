//
//  UIViewController.swift
//  iosapp
//
//  Created by wtccuser on 3/21/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import UIKit
extension UIViewController {
    func stopProgressIndicator (progressActivityIndicator : UIActivityIndicatorView) {
        progressActivityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        
        self.view.removeFromSuperview()
    }
    func progressIndicator(text: String,inout progressActivityIndicator : UIActivityIndicatorView ) {
          var strLabel = UILabel()
            var messageFrame = UIView()
         // progressActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        
        strLabel.text = text
        
        strLabel.textColor = UIColor.blueColor()
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        
        messageFrame.layer.cornerRadius = 15
        
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        //indicator.startAnimating()
        
       // activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        
       // activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        progressActivityIndicator.startAnimating()
        
        messageFrame.addSubview(progressActivityIndicator)
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
        
    }
}
//
