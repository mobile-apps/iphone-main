//
//  TableViewControllerDepartments.swift
//  iosapp
//
//  Created by wtccuser on 4/29/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import UIKit
class TableViewControllerDepartments : UITableViewController {
   
    @IBOutlet var departmentUITableView: UITableView!
    @IBAction func backButtonAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
   /*
    @IBAction func backBarButtonAction(sender: AnyObject) {
     
    }*/
     var departments = [Department]()
        let progressActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        let animator = Animator()
        animator.viewFlipFromRight(departmentUITableView, aniTime: 1)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //call web service
        let client = DepartmentClient()
        
        
        progressActivityIndicator.center = self.view.center
        view.addSubview(progressActivityIndicator)
        
        
        progressActivityIndicator.startAnimating()
        
        /**
         *  Call fetchPodcasts
         */
        client.fetchDepartments {
            (departments, error) in
            if let error = error {
                print("Error: \(error)")
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                }
                if let departments = departments {
                    /**
                     *  dispatch_async fixes slowness
                     */
                    dispatch_async(dispatch_get_main_queue(), {
                        self.departments = departments
                        self.tableView.reloadData()
                       self.progressActivityIndicator.stopAnimating()
                       self.progressActivityIndicator.hidden = true
                        
                        
                    })
                }
            }
            
            
        } //end of fetchPodcasts
     }//end of viewDidLoad
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departments.count
    }
    
    func lastWord(value: String) -> String {
        // Find index of space.
        var start = value.endIndex
        for var i = value.startIndex;
            i < value.endIndex;
            i = i.successor() {
                // If character is a space, the second word starts at the next index.
                if value[i] == "," {
                    // First char of second word is next index.
                    start = i.successor()
                    break
                }
        }
        // Return substring.
        return value[start..<value.endIndex]
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        // Set appropriate labels for the cells.
       // cell.textLabel?.text = departments[indexPath.row].name
        
        // let cell = tableView.dequeueReusableCellWithIdentifier("EventTableViewCell", forIndexPath: indexPath)
      
        
        // Set appropriate labels for the cells.
        // Set appropriate labels for the cells.
        // cell.textLabel?.text = events[indexPath.row].name
        //cell.myImageView.image = UIImage(named: "test.png")
        //mycell.generalinformation.adjustsFontSizeToFitWidth = NO;
       
        //mycell.startDate?.text = departments[indexPath.row].formatedStartDate
        var truncateString : String = departments[indexPath.row].generalInformation
        truncateString = truncateString.trunc(50)
        cell.detailTextLabel?.text = truncateString        
         cell.textLabel?.text = departments[indexPath.row].name
        
        
        
        
        // mycell.generalinformation?.text = "This is a short sentence. This is another short and large sentence. This is the last sentence."
        
        
        // mycell.generalinformation.lineBreakMode =NSLineBreakByTruncatingTail
        var icon = departments[indexPath.row].icon
        // Note this index is not an integer but an index into a character view (String.CharacterView.Index)
        icon = lastWord(icon)
        icon = icon.stringByReplacingOccurrencesOfString("\r\n", withString: "")
                   //remove any bogus characters
           // let positionBogus = icon.indexOf(",")
           // if (positionBogus > -1) {
             //   icon = icon.substring(positionBogus + 1,icon.length());
           // }
     
     let decodedData = NSData(base64EncodedString: icon, options: NSDataBase64DecodingOptions(rawValue: 0))
      let decodedimage = UIImage(data: decodedData!)
        cell.imageView?.image = decodedimage
        //mycell.textLabel?.text = "today"
        // mycell.eventimage.image = decodedimage
        //stopProgressIndicator(self.progressActivityIndicator)
        return cell
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("show_department_seque", sender: indexPath);
        
        
    }
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "show_department_seque" {
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailDepartmentViewController
            let row = (sender as! NSIndexPath).row; //we know that sender is an NSIndexPath here.
            let selectedDepartment = departments[row]
            
            controller.department = selectedDepartment
          
            // controller.view.backgroundColor = UIColor.blueColor()
            
        }
        
        
    }
    
}
