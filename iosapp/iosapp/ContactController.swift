//
//  ContactController.swift
//  iosapp
//
//  Created by wtccuser on 4/21/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import UIKit
import Social
import MessageUI
public class ContactController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBAction func mailAction(sender: AnyObject) {
        // Check if Mail is available
        if(MFMailComposeViewController.canSendMail()){
            // Create the mail message
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("New App")
            mail.setMessageBody("I want to share this App: ", isHTML: false)
            // Attach the image
            //mail.addAttachmentData(imageData!, mimeType: "image/png", fileName: "Image")
            self.presentViewController(mail, animated: true, completion: nil)
        } else {
            // Mail not available. Show a warning
            let alert = UIAlertController(title: "Email", message: "Email not available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    @IBAction func twitterAction(sender: AnyObject) {
        // Check if Twitter is available
        if(SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)) {
            // Create the tweet
            let tweet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            tweet.setInitialText("I want to share this App: ")
            tweet.addImage(UIImage(named: "shareImage"))
            self.presentViewController(tweet, animated: true, completion: nil)
        } else {
            // Twitter not available. Show a warning
            let alert = UIAlertController(title: "Twitter", message: "Twitter not available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    @IBAction func facebookAction(sender: AnyObject) {
        // Check if Facebook is available
        if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook))
        {
            // Create the post
            let post = SLComposeViewController(forServiceType: (SLServiceTypeFacebook))
            post.setInitialText("I want to share this App: ")
            post.addImage(UIImage(named: "shareImage"))
            self.presentViewController(post, animated: true, completion: nil)
        } else {
            // Facebook not available. Show a warning
            let alert = UIAlertController(title: "Facebook", message: "Facebook not available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }    }
    @IBAction func backAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    // Required by interface MFMailComposeViewControllerDelegate
    public func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        // Close the mail dialog
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}