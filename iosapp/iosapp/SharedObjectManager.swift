//
//  SharedObjectManager.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//
/// <#Description#>
import Foundation

typealias errorMessage = String

public class SharedObjectManager {
    
    enum OBJECT_NAME {
        
        case CATEGORIES, CRASH, EVENTS, GPSLOCATION, ORG, REGISTERUSER, REVIEWEDITEMS, SUBSCRIBER, DIRECTIONS, ISPENDING
        
    }
        
} //class