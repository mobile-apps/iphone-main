//
//  LeftMenu.swift
//  LeftSlideoutMenu
//
//  Created by Robert Chen on 8/5/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

class LeftMenu : BaseTabTableController, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate{
    /// master menu
    var _menuItems : [MenuItem] = []
    var delegate:AppDelegate?
    let customNavigationAnimationController = CustomNavigationAnimationController()
    let menuOptions = ["Open Modal", "Open Push"]
    var _errorMessage : String =  String();//initialize   
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        customNavigationAnimationController.reverse = operation == .Pop
        return customNavigationAnimationController
    }
    
    func test() {
       // UITabBarController
    }
  
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.leftTabIndex = 0
        self.rightTabIndex = 3
        navigationController?.delegate = self
        //navigationController?.delegate = self
        //Alert.Warning(self, message: "test")
         LogUtils.INFO(displayText: "starting viewDidLoad")
        do {
            try  _menuItems = MenuHelper.getMenuItems()
            
            
        } catch Application.ApplicationError.DatatypeConfigurationException(let message) {
            _errorMessage = message;
            LogUtils.SEVERE(displayText: message)
            
        }
        catch  {
            //  LogUtils.SEVERE("unknown error")
            
        }
        LogUtils.INFO(displayText: "ending viewDidLoad")
    }
    
}

// MARK: - UITableViewDelegate methods

extension LeftMenu {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let menuItem = _menuItems[indexPath.row]
     // UITabBarController; tbc = [self.storyboard ?.instantiateViewControllerWithIdentifier("")];
       
        print("\(menuItem.ShowEvent)")
        print("\(indexPath.row)")
       //contactViewController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        switch indexPath.row {
        case 0:
            
            let tbc = storyboard.instantiateViewControllerWithIdentifier("tabbarcontrolId") as! UITabBarController
            self.presentViewController(tbc, animated: true, completion: nil)
            tbc.selectedIndex=0;
            break
        case 1:
            
            let tbc = storyboard.instantiateViewControllerWithIdentifier("tabbarcontrolId") as! UITabBarController
            self.presentViewController(tbc, animated: true, completion: nil)
            tbc.selectedIndex=1;
            break
        case 2:
            
            let tbc = storyboard.instantiateViewControllerWithIdentifier("tabbarcontrolId") as! UITabBarController
            self.presentViewController(tbc, animated: true, completion: nil)
            tbc.selectedIndex=2;
            break
        case 3:
            
            let tbc = storyboard.instantiateViewControllerWithIdentifier("tabbarcontrolId") as! UITabBarController
            self.presentViewController(tbc, animated: true, completion: nil)
            tbc.selectedIndex=3;
            break
            
        //case 4:
            
            
           // let controller = storyboard.instantiateViewControllerWithIdentifier("contactViewController")
           // self.presentViewController(controller, animated: true, completion: nil)            // Both FirstViewController and SecondViewController listen for this
            //self.performSegueWithIdentifier("contactViewController", sender: self)
            break
        case 4:
            //tableControllerDepartments
            
            let controller = storyboard.instantiateViewControllerWithIdentifier("tableControllerDepartments")
            self.presentViewController(controller, animated: true, completion: nil)
            break
        default:
            print("indexPath.row:: \(indexPath.row)")
        }
        
        
        
        // tbc.selectedIndex=1;
       // [self presentViewController:tbc animated:YES completion:nil];
        
      //  self.performSegueWithIdentifier(menuItem.ShowEvent, sender: self)
     //   self.performSegueWithIdentifier("ddd", sender: self)
        
        /*
        switch indexPath.row {
        case 0:
            // ContainerVC.swift listens for this
            NSNotificationCenter.defaultCenter().postNotificationName("openModalWindow", object: nil)
        case 1:
            // Both FirstViewController and SecondViewController listen for this
            NSNotificationCenter.defaultCenter().postNotificationName("openPushWindow", object: nil)
        default:
            print("indexPath.row:: \(indexPath.row)")
        }
        */
        // also close the menu
       // NSNotificationCenter.defaultCenter().postNotificationName("closeMenuViaNotification", object: nil)
        
    }
    
}

// MARK: - UITableViewDataSource methods

extension LeftMenu {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _menuItems.count
    }
    
    override internal func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // menui
        return 1
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        LogUtils.INFO(displayText: "starting load table")
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        let menuItem = self._menuItems[indexPath.row]
        cell.textLabel?.text = menuItem.Title
        cell.accessoryType = .DisclosureIndicator
        cell.imageView!.image = UIImage(named: menuItem.ImageName)
        
        //let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
       // cell.textLabel?.text = menuOptions[indexPath.row]
        LogUtils.INFO(displayText: "ending load table")        
        return cell
    }
    
}
