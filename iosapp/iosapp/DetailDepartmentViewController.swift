//
//  DetailDepartmentViewController.swift
//  iosapp
//
//  Created by wtccuser on 5/3/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import UIKit
import CoreLocation
import MessageUI
class DetailDepartmentViewController : UITableViewController
    , CLLocationManagerDelegate, MFMailComposeViewControllerDelegate
{
    let locationManager = CLLocationManager()
    var phoneLatitude : Double = 0;
    var phoneLontitude : Double = 0;
    /**
     Go back to department list
     
     - parameter sender: <#sender description#>
     */
    @IBAction func backAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    let section = ["Name", "Description", "Address", "Phone", "Email Address"]
    var department : Department? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return self.section[section]
        
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return self.section.count
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return 1
        
    }
    /**
     user selected the row
     
     - parameter tableView: <#tableView description#>
     - parameter indexPath: <#indexPath description#>
     */
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        print("section:: \(indexPath.row)")
        switch indexPath.section {
        case 0:
           
            break
        case 1:
            break
        case 2:
           // cell.textLabel?.text = department?.location
 
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
                Map.openMapWithCoordinates((department?.longitude)!, destinationLat: (department?.latitude)!, sourceLon : phoneLontitude , sourceLat : phoneLatitude, destinationLocationName:(department?.name)!)
            }
            break
        case 3:
           // cell.textLabel?.text = department?.phone
            let phone : String = (department?.phone)!
            TelephoneHelper.callNumber(phone.digitsOnly())
            break
            
        case 4:
           // cell.textLabel?.text = department?._email
            // Check if Mail is available
            if(MFMailComposeViewController.canSendMail()){
                // Create the mail message
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setSubject("Need Help")
                mail.setMessageBody("I have a question: ", isHTML: false)
                mail.setToRecipients([(department?.email)!])
                // Attach the image
                //mail.addAttachmentData(imageData!, mimeType: "image/png", fileName: "Image")
                self.presentViewController(mail, animated: true, completion: nil)
            } else {
                // Mail not available. Show a warning
                let alert = UIAlertController(title: "Email", message: "Email not available", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
           
            break
        default:
            print("indexPath.row:: \(indexPath.row)")
            
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        // Configure the cell...
        
      //  cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = department?.name
            break
        case 1:
            cell.textLabel!.numberOfLines = 0;
            cell.textLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
            cell.textLabel?.text = department?.generalInformation
            break

        case 2:
            cell.textLabel?.text = department?.location
           cell.accessoryType = .DisclosureIndicator
            break
        case 3:
            cell.textLabel?.text = department?.phone
            cell.accessoryType = .DisclosureIndicator
            break

        case 4:
            cell.textLabel?.text = department?._email
            cell.accessoryType = .DisclosureIndicator
            break
        default:
            print("indexPath.row:: \(indexPath.row)")
            
        }
        return cell
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        phoneLatitude = locValue.latitude
        phoneLontitude = locValue.longitude
        
    }

}