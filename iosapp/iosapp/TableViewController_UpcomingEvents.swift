//
//  TableViewController_UpcomingEvents.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/12/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit




class TableViewController_UpcomingEvents: BaseTabTableController {
    
    // MARK: - Table view data source
      var events = [Event]()
     let progressActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    
    func refresh(sender: UIBarButtonItem) {
        viewDidLoad();
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBtn: UIButton = UIButton()
        myBtn.setImage(UIImage(named: "refresh"), forState: .Normal)
        myBtn.frame = CGRectMake(0, 0, 70, 70)
        myBtn.addTarget(self, action: "refresh:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: myBtn), animated: true)
        //assign button to navigationbar
       // self.navigationItem.rightBarButtonItem = barButton
        
        
        //set progess bar
       // progressIndicator("loading")
       
        //progressActivityIndicator.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
     //   progressActivityIndicator.frame.center = view.center
        progressActivityIndicator.center = self.view.center
        view.addSubview(progressActivityIndicator)
        
        
        progressActivityIndicator.startAnimating()
        
        //progressIndicator("Loading", progressActivityIndicator: progressActivityIndicator)
        //call web service
        let eventClient = EventRestfulClient()
        
        
    
        
        
        /**
        *  Call fetchPodcasts
        */
        eventClient.fetchEvents {
            (events, error) in
            if let error = error {
                print("Error: \(error)")
                self.progressActivityIndicator.stopAnimating()
                self.progressActivityIndicator.hidden = true

                
                // create the alert
                let alert = UIAlertController(title: "Oops Error", message: " \(error)", preferredStyle: UIAlertControllerStyle.Alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                // show the alert
                self.presentViewController(alert, animated: true, completion: nil)
                

            } else {
                dispatch_async(dispatch_get_main_queue()) {
                }
                if let events = events {
                    /**
                    *  dispatch_async fixes slowness
                    */
                    dispatch_async(dispatch_get_main_queue(), {
                        self.events = events
                        self.tableView.reloadData()
                        self.progressActivityIndicator.stopAnimating()
                        self.progressActivityIndicator.hidden = true

                        
                    })
                }
            }
            
            
        } //end of fetchPodcasts
        //stopProgressIndicator(self.progressActivityIndicator)
    }//end of method
    
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       // let cell = tableView.dequeueReusableCellWithIdentifier("EventTableViewCell", forIndexPath: indexPath)
        
        let mycell: EventTableViewCell = tableView.dequeueReusableCellWithIdentifier("EventTableViewCell") as! EventTableViewCell

        // Set appropriate labels for the cells.
        // Set appropriate labels for the cells.
       // cell.textLabel?.text = events[indexPath.row].name
        //cell.myImageView.image = UIImage(named: "test.png")
       //mycell.generalinformation.adjustsFontSizeToFitWidth = NO;
    mycell.generalinformation.lineBreakMode = .ByTruncatingTail
        mycell.address?.text = events[indexPath.row].location
               mycell.startDate?.text = events[indexPath.row].formatedStartDate
        var truncateString : String = events[indexPath.row].generalInformation
        truncateString = truncateString.trunc(50)
      mycell.generalinformation?.text = truncateString
        //ellipsis
        mycell.generalinformation.adjustsFontSizeToFitWidth = false
        mycell.generalinformation.lineBreakMode = .ByTruncatingTail
        mycell.generalinformation.numberOfLines = 0
      //  mycell.generalinformation.adjustsFontSizeToFitWidth = true;
        mycell.generalinformation.sizeToFit()
      // mycell.generalinformation?.text = "This is a short sentence. This is another short and large sentence. This is the last sentence."
        
      
       // mycell.generalinformation.lineBreakMode =NSLineBreakByTruncatingTail
        
        print(events[indexPath.row].department.icon)
        let decodedData = NSData(base64EncodedString: events[indexPath.row].department.icon, options: NSDataBase64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: decodedData!)
       
        mycell.departmentImage.image = decodedimage
        //mycell.textLabel?.text = "today"
        // mycell.eventimage.image = decodedimage
        //stopProgressIndicator(self.progressActivityIndicator)
        return mycell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showDetail1fromTableView_UpcomingEvents", sender: indexPath);
   
        
    }
    
    
    // MARK: - Navigation
    
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showDetail1fromTableView_UpcomingEvents" {
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController_UpcomingEvents
            let row = (sender as! NSIndexPath).row; //we know that sender is an NSIndexPath here.
            let selectedEvent = events[row]
            
            controller.detailEvent = selectedEvent
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
           // controller.view.backgroundColor = UIColor.blueColor()
            
        }
        
        
    }
    
    
}
