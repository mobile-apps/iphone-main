//
//  TableViewController_Podcasts.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/13/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit


class TableViewController_Podcasts: BaseTabTableController {
    
    var podcasts = [Podcast]()
 var progressActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leftTabIndex = 3
        self.rightTabIndex = 1
        progressActivityIndicator.center = self.view.center
        view.addSubview(progressActivityIndicator)
        progressActivityIndicator.startAnimating()
        //call web service
        let podcastClient = PodcastClient(hostName: "https://jbossewsalex-javauniversity.rhcloud.com")
        
        /**
        *  Call fetchPodcasts
        */
        podcastClient.fetchPodcasts {
            (podcasts, error) in
            if let error = error {
                print("Error: \(error)")
                
                self.progressActivityIndicator.stopAnimating()
                self.progressActivityIndicator.hidden = true
                
                
                // create the alert
                let alert = UIAlertController(title: "Oops Error", message: " \(error)", preferredStyle: UIAlertControllerStyle.Alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                // show the alert
                self.presentViewController(alert, animated: true, completion: nil)
                
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                }
                if let podcasts = podcasts {
                    /**
                    *  dispatch_async fixes slowness
                    */
                    dispatch_async(dispatch_get_main_queue(), {
                        self.podcasts = podcasts
                        self.tableView.reloadData()
                        self.progressActivityIndicator.stopAnimating()
                    })
                }
            }
            
             
        } //end of fetchPodcasts
        
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return podcasts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        // Set appropriate labels for the cells.
        cell.textLabel?.text = podcasts[indexPath.row].title
        cell.detailTextLabel?.text = podcasts[indexPath.row].description
        //cell.description.  = podcasts[indexPath.row].description
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
     self.performSegueWithIdentifier("showDetail1fromTableView_Podcasts", sender: self)
       
        
    }
    
    
    // MARK: - Navigation
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        
//        if segue.identifier == "showDetail1fromTableView_Podcasts" {
//            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController_Podcast
//            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
//            controller.navigationItem.leftItemsSupplementBackButton = true
//            controller.view.backgroundColor = UIColor.blueColor()
//            
//        }
//        
//    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail1fromTableView_Podcasts" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = podcasts[indexPath.row]
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController_Podcast
//                if let oldController = detailViewController {
//                    controller.languageString = oldController.languageString
//                }
                
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
                //detailViewController = controller
            }
        }
    }
    
}