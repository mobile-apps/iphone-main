//
//  Step.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation


public class Step {
    
    class var rootProperty : String {
        get {
            return "steps"
        }
    }
    
    
    //Define json column names used to parse json object
    class var distanceProperty : String {
        get {
            return "distance"
        }
    }
    
    
    class var durationProperty : String {
        get {
            return "duration"
        }
    }
    
    
    class var manueverProperty : String {
        get {
            return "manuever"
        }
    }
    
    
    class var htmlInstructionProperty : String {
        get {
            return "htmlInstruction"
        }
    }
    
    
    var _distance : String!
    var distance : String {
        get {
            return _distance
        }
        set {
            self._distance = newValue
        }
    }
    
    
    var _duration : String!
    var duration : String {
        get {
            return _duration
        }
        set {
            self._duration = newValue
        }
    }
    
    
    var _manuever : String!
    var manuever : String {
        get {
            return _manuever
        }
        set {
            self._manuever = newValue
        }
    }
    
    
    var _htmlInstruction : String!
    var htmlInstruction : String {
        get {
            return _htmlInstruction
        }
        set {
            self._htmlInstruction = newValue
        }
    }
    
    
    //Constructors
    init (distance : String, duration : String, manuever : String, htmlInstruction : String) {
        
        self._distance = distance
        self._duration = duration
        self._manuever = manuever
        self._htmlInstruction = htmlInstruction
        
    }
    
    
    init (distance : String, duration : String, htmlInstruction : String) {
        
        self._distance = distance
        self._duration = duration
        self._htmlInstruction = htmlInstruction
        
    }
    
} //class
