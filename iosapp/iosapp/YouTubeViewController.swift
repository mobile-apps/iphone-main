//
//  YouTubeViewController.swift
//  iosapp
//
//  Created by wtccuser on 5/26/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import UIKit

class YouTubeViewController: UIViewController {
    
    @IBOutlet weak var youTubePlayerView: YouTubePlayerView!
    func loadYouTube() {
        youTubePlayerView.playerVars = [
            "playsinline": "1",
            "controls": "0",
            "showinfo": "0"
        ]
        
        
        youTubePlayerView.loadVideoID("nZ-OPZpW2lk")
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let progressActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
               progressActivityIndicator.center = self.view.center
        view.addSubview(progressActivityIndicator)
        
        
        progressActivityIndicator.startAnimating()
        loadYouTube()
        progressActivityIndicator.stopAnimating()
        progressActivityIndicator.hidden = true
        //
        // Do any additional setup after loading the view, typically from a nib.
    }
    
       
    
}

