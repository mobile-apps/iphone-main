//
//  Application.swift
//  mobileapplication
//
//  Created by wtccuser on 1/3/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import Foundation
public extension NSBundle {
    
    class var applicationVersionNumber: String {
        if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return "Version Number Not Available"
    }
    
    class var applicationBuildNumber: String {
        if let build = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        return "Build Number Not Available"
    }
    
}

/// Application level stuff
public class Application {
    /**
     Application exception
     
     - ApplicationException:            is used for reporting application level exceptions
     - DatatypeConfigurationException: indicates a serious configuration error.
     - ExecutionException:             Exception thrown when attempting to retrieve the result of a task that aborted by throwing an exception
     - GeneralSecurityException:       is a generic security exception class that provides type safety for all the security-related exception
     - IOException:                    Signals that an I/O exception of some sort has occurred.
     - ParseException:                 Signals that an error has been reached unexpectedly while parsing
     - SOAPException:                  An exception that signals that a SOAP exception has occurred.
     */
    enum ApplicationError: ErrorType {
        case ApplicationException(message:String)
        case DatatypeConfigurationException (message:String)
        case ExecutionException(message:String)
        case GeneralSecurityException (message:String)
        case IOException (message:String)
        case ParseException (message:String)
        case SOAPException (message:String)
    }
}
