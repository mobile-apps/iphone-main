//
//  EventTableViewCell.swift
//  iosapp
//
//  Created by wtccuser on 1/24/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//
import Foundation
import UIKit
/// Custom table view  cell used to display the event                      
public class EventTableViewCell:UITableViewCell {
   
    
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var departmentImage: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var eventimage: UIImageView!
   

    @IBOutlet weak var generalinformation: UILabel!
}
