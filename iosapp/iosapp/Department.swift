//
//  Department.swift
//  mobileapplication
//
//  Created by wtccuser on 8/18/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation
/*

*/
//typealias Location = String
typealias Longitude = Double
typealias Latitude = Double
typealias Icon = String
typealias Email = String
typealias Name = String
typealias Phone = String
typealias DepartmentID = Int
typealias GeneralInformation = String

public class Department {
    class var RootProperty : String {
        get {
            return "departments"
        }
    }
   //define json column names used to parse json object
    class var NameProperty : String {
        get {
            return "name"
        }
    }
    
    class var PhoneProperty : String {
        get {
            return "phone"
        }
    }
    class var EmailProperty : String {
        get {
            return "email"
        }
    }
    class var IconProperty : String {
        get {
            return "icon"
        }
    }
    class var DepartmentIDProperty : String {
        get {
            return "departmentId"
        }
    }
    
    class var LocationProperty : String {
        get {
            return "location"
        }
    }
    
    class var GeneralInformationProperty : String {
        get {
            return "generalInformation"
        }
    }
    class var LatitudeProperty : String {
        get {
            return "latitude"
        }
    }
    class var LongitudeProperty : String {
        get {
            return "longitude"
        }
    }
    
    var _location : String!
    var location : String {
        get {
            return _location
        }
        set {
            self._location = newValue
        }
    }
    
    var _longitude : Double!
    var longitude : Double {
        get {
            return _longitude
        }
        set {
            self._longitude = newValue
        }
    }
    
    var _latitude : Double!
    var latitude : Double {
        get {
            return _latitude
        }
        set {
            self._latitude = newValue
        }
    }
    
    var _icon : String!
    var icon : String {
        get {
            return _icon
        }
        set {
            self._icon = newValue
        }
    }
    
    var _email : String!
    var email : String {
        get {
            return _email
        }
        set {
            self._email = newValue
        }
    }
    
    var _name : String!
    var name : String {
        get {
            return _name;
        }
        set {
            self._name = newValue
        }
    }
    
    var _phone: String!
    var phone: String {
        get {
            return _phone
        }
        set {
            self._phone  = newValue
        }
    }
    
    var _departmentid : Int!
    var departmentid : Int {
        get {
            return _departmentid
        }
        set {
            self._departmentid = newValue
        }
    }
    
    var _generalInformation :String!
    var generalInformation : String {
        get {
            return _generalInformation
        }
        set {
            self._generalInformation = newValue
        }
    }
    
    
    //Constructor
    //init(location:Location, longitude:Longitude, latitude:Latitude, icon:Icon, email:Email, name:Name, phone:Phone, departmentid: DepartmentID, generalInformation:GeneralInformation) {
    init(location:String,longitude:Double, latitude:Double, icon:String, email:String, name:String, phone:String, departmentid: Int, generalInformation:String) {
        self._location = location
        self._longitude = longitude
        self._latitude = latitude
        self._icon = icon
        self._email = email
        self._name = name
        self._phone = phone
        self.departmentid = departmentid
        self._generalInformation = generalInformation
    }
    
}
