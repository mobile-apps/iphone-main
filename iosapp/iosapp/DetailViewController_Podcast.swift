//
//  DetailViewController_Podcast.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/13/15.
//  Copyright Â© 2015 wtccuser. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

/// <#Description#>
class DetailViewController_Podcast: UIViewController, AVAudioPlayerDelegate, NSURLSessionDownloadDelegate {
     static var downloadComplete  = false
     static var audioPlayer = AVAudioPlayer()
    
    internal static func globalStop() {
        if DetailViewController_Podcast.downloadComplete {
            
            if DetailViewController_Podcast.audioPlayer.playing {
                DetailViewController_Podcast.audioPlayer.stop()
            }
        }

    }
    
    @IBAction func pauseAction(sender: AnyObject) {
        DetailViewController_Podcast.globalStop()
        
        
        
    }
    @IBAction func startAction(sender: AnyObject) {
        if DetailViewController_Podcast.downloadComplete {
            if DetailViewController_Podcast.audioPlayer.playing == false {
                DetailViewController_Podcast.audioPlayer.play()
            }
        }
    }
    @IBOutlet weak var playerControlStackView: UIStackView!
    @IBOutlet weak var durationUILabel: UILabel!
    
    @IBOutlet weak var timerUILabel: UILabel!
    
    @IBOutlet weak var playerUISlider: UISlider!
    
    @IBAction func stopAction(sender: AnyObject) {
        goBack()
    }
    func playerInformation() {
        
        LogUtils.ENTRY_LOG()
        playerUISlider.minimumValue = 0
        playerUISlider.maximumValue = 100 //percentage
        let  formatTime = stringFromTimeInterval(DetailViewController_Podcast.audioPlayer.duration)
        durationUILabel.text = formatTime as String
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateTime", userInfo: nil, repeats: true)
        self.updater = CADisplayLink(target: self, selector: Selector("trackAudio"))
        self.updater.frameInterval = 1
        self.updater.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        LogUtils.EXIT_LOG()
        
    }
    @IBOutlet weak var progressVew: ProgressView!
    // @IBOutlet weak var dateUILabel: UILabel!
    
    @IBOutlet weak var descriptionUITextView: UITextView!
    // @IBOutlet weak var descriptionUITextView: UITextView!
    @IBOutlet weak var dateUILabel: UILabel!
    @IBAction func stopPlayerAction(sender: AnyObject) {
        LogUtils.ENTRY_LOG()
        
        // playerUILabel.text = "Stopping sermon"
        self.goBack()
    }
    @IBOutlet weak var descriptionUILabel: UILabel!
    
    // @IBOutlet weak var topView: ProgressView!
    // @IBOutlet weak var playerUILabel: UILabel!
    //@IBOutlet weak var playerUISlider: UISlider!
    
    var timer:NSTimer!
    var updater : CADisplayLink! = nil
    var rightButton : UIBarButtonItem! = nil
   
    
    var task : NSURLSessionTask!
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        return session
    }()
    /**
     Hide content
     */
    func hideContent() {
        LogUtils.ENTRY_LOG()
        self.descriptionUITextView.text = ""
        self.dateUILabel.text = ""
        LogUtils.EXIT_LOG()
    }
    var detailItem: Podcast?
    /**
     Download Sermon mp3 file
     */
    func downloadSermon() {
        LogUtils.ENTRY_LOG()
        
        DetailViewController_Podcast.globalStop()
        hideContent()
        durationUILabel.text = "Downloading"
        self.timerUILabel.text = ""
        // playerUILabel.text = "Downloading \(detailItem?.title)"
        playerUISlider.minimumValue = 0
        playerUISlider.maximumValue = 100 //percentage
        
       let url = detailItem?.mediaLocation
        //let url = "http://www.stephaniequinn.com/Music/Allegro%20from%20Duet%20in%20C%20Major.mp3"
        LogUtils.INFO(displayText: "starting downloadSermon \(url)")
        
        let fileURL = NSURL(string:url!)
        
        let req = NSMutableURLRequest(URL:fileURL!)
        req.timeoutInterval = 120
        // req.timeoutIntervalForResource =  120
        let task = self.session.downloadTaskWithRequest(req)
        self.task = task
        task.resume()
        LogUtils.EXIT_LOG()
        
        
        
    }
    /**
     Set default gui settings
     */
    func configureView() {
        LogUtils.ENTRY_LOG()
        descriptionUITextView.text = detailItem?.description
        dateUILabel.text = detailItem?.publishDate
        LogUtils.EXIT_LOG()
    }
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        LogUtils.ENTRY_LOG()
        let progress = Float(writ) / Float(exp)
        progressVew.animateProgressViewToProgress(progress)
        progressVew.updateProgressViewLabelWithProgress(progress * 100)
        LogUtils.EXIT_LOG()
        //topView.animateProgressView()
        //playerUILabel.text = "downloading"
        //print(" didWriteData downloaded ")
    }
    /**
     <#Description#>
     
     - parameter session:            <#session description#>
     - parameter downloadTask:       <#downloadTask description#>
     - parameter fileOffset:         <#fileOffset description#>
     - parameter expectedTotalBytes: <#expectedTotalBytes description#>
     */
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    
    /**
     This is the ending download mp3 image
     
     - parameter session:      <#session description#>
     - parameter downloadTask: <#downloadTask description#>
     - parameter location:     <#location description#>
     */
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        LogUtils.ENTRY_LOG()
        
        progressVew.hideProgressView()
        durationUILabel.text = "Downloading"
        let response = downloadTask.response as! NSHTTPURLResponse
        let stat = response.statusCode
        print("status \(stat)")
        if stat != 200 {
            return
        }
        DetailViewController_Podcast.downloadComplete = true
        do {
            self.configureView()
            //get or add to session
            if let object = SessionHelper.get(SessionName.AudioPlayer)
            {
                //self.audioPlayer = object as! AVAudioPlayer
            } else {
                //add to session
                let soundData = NSData(contentsOfURL:location)!
                //  try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                //AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: [])
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: [])
                try AVAudioSession.sharedInstance().setActive(true)
                // try AVAudioSession.sharedInstance().s
                DetailViewController_Podcast.audioPlayer = try AVAudioPlayer(data: soundData)
                DetailViewController_Podcast.audioPlayer.numberOfLoops = -1
                DetailViewController_Podcast.audioPlayer.prepareToPlay()
                DetailViewController_Podcast.audioPlayer.volume = 1.0
                DetailViewController_Podcast.audioPlayer.delegate = self
                DetailViewController_Podcast.audioPlayer.prepareToPlay()
                DetailViewController_Podcast.audioPlayer.play()
                
                
                let audioInfo = MPNowPlayingInfoCenter.defaultCenter()
                var nowPlayingInfo:[NSObject:AnyObject] = [:]
                
                nowPlayingInfo[MPMediaItemPropertyTitle] = detailItem?.description
                
                let mpic = MPNowPlayingInfoCenter.defaultCenter()
                mpic.nowPlayingInfo = [
                    MPMediaItemPropertyTitle:(detailItem?.description)!,
                    MPMediaItemPropertyArtist:(detailItem?.title)!
                ]
                // audioInfo.nowPlayingInfo = detailItem?.description
                // SessionHelper.add(SessionName.AudioPlayer, object: self.audioPlayer)
            }
            
            
            
            
            
            LogUtils.DEBUG(displayText: "playing")
            playerInformation()
            //let  formatTime = stringFromTimeInterval(audioPlayer.duration)
            LogUtils.EXIT_LOG()
            
        } catch {
            // LogUtils.SEVERE(displayText: / error)
            //print(error)
              LogUtils.SEVERE(displayText: "Error \(error)")
            
        }
    }
    
    
    // ======== respond to remote controls
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
    }
    /**
     <#Description#>
     
     - parameter event: <#event description#>
     */
    override func remoteControlReceivedWithEvent(event: UIEvent?) { // *
        let rc = event!.subtype
        let p = DetailViewController_Podcast.audioPlayer
        print("received remote control \(rc.rawValue)") // 101 = pause, 100 = play
         stopOrPlay()
        /*
        switch rc {
        case .RemoteControlTogglePlayPause:
            if p.playing { p.pause() } else { p.play() }
        case .RemoteControlPlay:
            p.play()
        case .RemoteControlPause:
            p.pause()
        default:break
 
            
            
            
        } */
        
    }
    
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if error != nil {
            LogUtils.SEVERE(displayText: "Error \(error)")
            LogUtils.EXIT_LOG()
            // create the alert
            let alert = UIAlertController(title: "Oops Error", message: " \(error)", preferredStyle: UIAlertControllerStyle.Alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            // show the alert
            self.presentViewController(alert, animated: true, completion: nil)
        }
       
    }
    
    /**
     Calculate time remaining
     */
    func updateTime() {
        LogUtils.ENTRY_LOG()
        //let timeLeft : NSTimeInterval  = self.audioPlayer.duration - self.audioPlayer.currentTime
        
        // let minutes = timeLeft/60;
        
        // let seconds = (timeLeft) % 60;
        
        let currentTime = Int(DetailViewController_Podcast.audioPlayer.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
        
        timerUILabel.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        LogUtils.EXIT_LOG()
    }
    
    /**
     Update slider
     */
    func trackAudio() {
        LogUtils.ENTRY_LOG()
        let normalizedTime = Float(DetailViewController_Podcast.audioPlayer.currentTime * 100.0 / DetailViewController_Podcast.audioPlayer.duration)
        
        // print("Upgrading progress \(normalizedTime)")
        playerUISlider.value = normalizedTime
        LogUtils.EXIT_LOG()
    }
    /**
     Return to sender
     */
    func goBack(){
        LogUtils.ENTRY_LOG()
        
        //if (self.audioPlayer.playing)  {
        //   self.audioPlayer.stop()
        
        //}
        
        
        self.dismissViewControllerAnimated(true, completion: nil)
        LogUtils.EXIT_LOG()
    }
    /**
     <#Description#>
     
     - parameter interval: <#interval description#>
     
     - returns: <#return value description#>
     */
    func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        LogUtils.ENTRY_LOG()
        let ti = NSInteger(interval)
        
        let ms = Int((interval % 1) * 1000)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        LogUtils.EXIT_LOG()
        return NSString(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms)
        
    }
    func stopOrPlay() {
       
        
        if DetailViewController_Podcast.downloadComplete {
            
            if DetailViewController_Podcast.audioPlayer.playing {
                self.rightButton.title = "Play"
                DetailViewController_Podcast.audioPlayer.stop()
            } else {
                self.rightButton.title = "Stop"
                DetailViewController_Podcast.audioPlayer.play()
            }
        }
        
    }
    /**
     main method
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        LogUtils.ENTRY_LOG()
        self.title = detailItem?.title
        // playerUISlider.minimumValue = 0
        //playerUISlider.maximumValue = 100 //percentage
        /// add back button
        let navigationBar = navigationController!.navigationBar
        navigationBar.tintColor = UIColor.blueColor()
        rightButton =  UIBarButtonItem(title: "Stop", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DetailViewController_Podcast.stopOrPlay))
        let leftButton =  UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DetailViewController_Podcast.goBack))
        navigationItem.leftBarButtonItem = leftButton
  
        navigationItem.rightBarButtonItem = rightButton
       
        // Do any additional setup after loading the view, typically from a nib.
        //self.configureView()
        self.downloadSermon()
        LogUtils.EXIT_LOG()
    }
    
    
    
}

