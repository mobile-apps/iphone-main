//
//  String.swift
//  iosapp
//
//  Created by wtccuser on 3/12/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import Foundation
extension String {
    
    var length : Int {
        return self.characters.count
    }
    
    func digitsOnly() -> String{
        let stringArray = self.componentsSeparatedByCharactersInSet(
            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let newString = stringArray.joinWithSeparator("")
        
        return newString
    }
    func trunc(length: Int, trailing: String? = "...") -> String {
        if self.characters.count > length {
            return self.substringToIndex(self.startIndex.advancedBy(length)) + (trailing ?? "")
        } else {
            return self
        }
    }
}
