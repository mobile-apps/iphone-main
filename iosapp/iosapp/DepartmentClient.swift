      //
//  DepartmentClient.swift
//  mobileapplication
//
//  Created by wtccuser on 8/19/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

//return array of departments and error code
typealias FetchDataCompletion = ([Department]?, NSError?) -> Void
//@brief: Use it to write a short description about the method, property, class, file, struct, or enum you’re documenting. No line breaks are allowed.
class DepartmentClient : NSObject {
    //let hostName : String //the is restful service host name
    //todo this should be read from the configuration file
    
  
    //define the url address
    var urlAddress : NSURL {
      //  let urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getAllDepartments"
        let urlpath = URLHelper.getURLAddress( URLAddress.GetAllDepartments)
            let url:NSURL = NSURL(string: urlpath)!
            return url
    //    return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getAllDepartments")! //todo the path should read from config file
    }
   
    /**
    * Invoke the JSOn web service to return a list of departments
    *
    * @return No return value
    */
    func fetchDepartments(completionHandler: FetchDataCompletion) {
        LogUtils.ENTRY_LOG()
        let urlPath = URLHelper.getURLAddress(URLAddress.GetAllDepartments)
        print(urlPath)
        let url: NSURL = NSURL(string: urlPath)!
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            /**
             *  If there is an error in the web request, print it to the console
             */
            if error != nil {
                print(error!.localizedDescription)
                completionHandler(nil, error)
            }
            
            
            do {
                // Try parsing some valid JSON
                let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                
               // print(jsonResult)
                let json = JSON(jsonResult)
                print(json)
                let count: Int? = json["departments"].array?.count
                LogUtils.DEBUG(displayText: "found \(count!) departments")
               // print("found \(count!) departments")
                
             //   let test = json["departments"].arrayObject
                var departments = [Department]()
                /// Check if any podcasts were returned
                if let x = count {
                    /**
                     *  If podcasts were returned, then loop through and print the Title of each podcast
                     */
                    for index in 0...x-1 {
                        if  let depart = json["departments"][index].dictionaryObject  {
                           let emailJson =  depart[Department.EmailProperty] as! String
                           let nameJson = depart[Department.NameProperty] as! String
                            let iconJson =  depart[Department.IconProperty] as! String
                            let phoneJson = depart[Department.PhoneProperty]as! String
                            let locationJson =  depart[Department.LocationProperty] as! String
                            let latitudeJson = depart[Department.LatitudeProperty]as! Double
                            let idJson =  depart[Department.DepartmentIDProperty] as! Int
                            let longitudeJson = depart[Department.LongitudeProperty]as! Double
                            let generalInformationJson = depart[Department.GeneralInformationProperty] as! String
                           
                            let department = Department(location: locationJson, longitude: longitudeJson,
                                latitude: latitudeJson, icon: iconJson, email: emailJson, name: nameJson, phone: phoneJson,
                                departmentid: idJson, generalInformation: generalInformationJson )
                            
                            departments.append(department)
                             //init(location:String,longitude:Double, latitude:Double, icon:String, email:String, name:String, phone:String, departmentid: Int, generalInformation:String)                             //let department = Department(location: location, )
                        }
                       
                    }
                }
                
                completionHandler(departments, nil)
                
                
            }
            catch let error as NSError? {
                print("A JSON parsing error occurred, here are the details:\n \(error)")
                completionHandler(nil, nil)
            }
            
        })
        task.resume()
       
         LogUtils.EXIT_LOG()
       
    }//end of fetchDepartments

    

}//end of class