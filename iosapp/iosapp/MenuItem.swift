//
//  MenuItem.swift
//  mobileapplication
//
//  Created by wtccuser on 10/23/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import Foundation
public class MenuItem {
    var _title :String!
    /// <#Description#>
    var Title : String {
        get {
            return _title
        }
        set {
            self._title = newValue
        }
    }
    var _position : Int!
    var Position : Int {
        get {
            return _position
        }
        set {
            self._position = newValue
        }
    }
    var _imageName : String!
    var ImageName : String {
        get {
            return _imageName
        }
        set {
            self._imageName = newValue
        }
    }
    var _showEvent : String!
    var ShowEvent : String {
        get {
            return _showEvent
        }
        set {
            self._showEvent = newValue
        }
    }
}
