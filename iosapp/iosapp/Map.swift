//
//  Map.swift
//  iosapp
//
//  Created by wtccuser on 3/25/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit
class Map {
    class func openMapWithCoordinates(destinationLon: Double, destinationLat: Double, sourceLon : Double , sourceLat : Double, destinationLocationName:String) {
        
        let stringLonFromDouble:String = String(format:"%f", destinationLon)
        let stringLatFromDouble:String = String(format:"%f", destinationLat)
        
        let stringSourceLonFromDouble:String = String(format:"%f", sourceLon)
        let stringSourceLatFromDouble:String = String(format:"%f", sourceLat)
        
        //openMapWithCoordinates(stringLonFromDouble, destinationLat: stringLatFromDouble, sourceLon : stringSourceLonFromDouble, sourceLat : stringLatFromDouble, StringdestinationLocationName:destinationLocationName)
         openMapWithCoordinates(stringLonFromDouble, destinationLat: stringLatFromDouble, sourceLon : stringSourceLonFromDouble, sourceLat : stringSourceLatFromDouble , destinationLocationName:destinationLocationName)
    }
    /**
     <#Description#>
     
     - parameter phoneLon:       <#phoneLon description#>
     - parameter phoneLat:       <#phoneLat description#>
     - parameter destinationLon: <#destinationLon description#>
     - parameter destinationLat: <#destinationLat description#>
     */
    class func openMapWithCoordinates(destinationLon: String, destinationLat: String, sourceLon : String, sourceLat : String , destinationLocationName:String) {
        
        let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(destinationLat)!, CLLocationDegrees(destinationLon)!)
       
        let placemark : MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary:nil)
        //35.794519035 lat
        //35° 47' 40.27'' N,78° 36' 6.08'' W
        //Longitude: -78.6016890
        let mapItem:MKMapItem = MKMapItem(placemark: placemark)
        
        mapItem.name = destinationLocationName
        
         let launchOptions:NSDictionary = NSDictionary(object: MKLaunchOptionsDirectionsModeDriving, forKey: MKLaunchOptionsDirectionsModeKey)
        
        //let coordinateSoure = CLLocationCoordinate2DMake(CLLocationDegrees(sourceLon)!, CLLocationDegrees(sourceLat)!)
        
         let coordinateSoure = CLLocationCoordinate2DMake(CLLocationDegrees(sourceLat)!, CLLocationDegrees(sourceLon)!)
        
        let placemarkSource : MKPlacemark = MKPlacemark(coordinate: coordinateSoure, addressDictionary:nil)
        
       let mapItemSource:MKMapItem = MKMapItem(placemark: placemarkSource)
        mapItemSource.name = "my current location"
        //mapItem.openInMapsWithLaunchOptions(nil)
       // let currentLocationMapItem:MKMapItem = MKMapItem.mapItemForCurrentLocation()
        
        MKMapItem.openMapsWithItems([mapItemSource, mapItem], launchOptions: launchOptions as? [String : AnyObject])
    }
}
