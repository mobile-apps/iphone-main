//
//  CrashReport.swift
//  mobileapplication
//
//  Created by wtccuser on 1/4/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//
import UIKit
import Foundation
public class CrashReport {
        
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func log(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        
        LogUtils.sharedInstance.Log.debug(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
      //  let modelName = UIDevice.currentDevice().modelName
        
        let urlPath = URLHelper.getURLAddress(URLAddress.GetCrashReport)
        let request = NSMutableURLRequest(URL: NSURL(string: urlPath)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        let uuid = NSUUID().UUIDString
        let params = ["appVersionCode":NSBundle.applicationVersionNumber
            ,"reportId":uuid
            ,"appVersionName":NSBundle.applicationBuildNumber
            ,"phoneModel":UIDevice.currentDevice().model
            ,"brand":UIDevice.currentDevice().name
            ,"deviceId":UIDevice.currentDevice().identifierForVendor!.UUIDString
            ,"packageName":functionName
            ,"logCat":displayText
            ,"androidVersion":UIDevice.currentDevice().systemVersion
            , "stackTrace":""  ] as Dictionary<String, String>

        /**
        *  <#Description#>
        */
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
        } catch {
            //handle error. Probably return or mark function as throws
          //  mlog.severe(error)
            LogUtils.SEVERE(displayText: "Error: \(error)")
          
           // print(error)
            return
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        /// <#Description#>
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            // handle error
            guard error == nil else { return }
           LogUtils.DEBUG(displayText: "Response: \(response)")
            
            let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            LogUtils.DEBUG(displayText: "Body: \(strData)")
            
            let json: NSDictionary?
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary
            } catch let dataError {
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                print(dataError)
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                LogUtils.SEVERE(displayText:"Error could not parse JSON: '\(jsonStr)'")
                // return or throw?
                return
            }
            
            
            // The JSONObjectWithData constructor didn't return an error. But, we should still
            // check and make sure that json has a value using optional binding.
            if let parseJSON = json {
                // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                let success = parseJSON["success"] as? Int
                LogUtils.DEBUG(displayText:"Succes: \(success)")
            }
            else {
                // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                LogUtils.SEVERE(displayText: "Error could not parse JSON: \(jsonStr)")
            }
            
        })
        
        task.resume()
        

        
    }
}
