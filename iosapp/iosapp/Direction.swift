//
//  Direction.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation


public class Direction {
    
    class var rootProperty : String {
        get {
            return "direction"
        }
    }
    
    
    //Define json column names used to parse json object
    class var startingAddressProperty : String {
        get {
            return "startingAddress"
        }
    }
    
    
    class var endingAddressProperty : String {
        get {
            return "endingAddress"
        }
    }

    
    class var stepsProperty : String {
        get {
            return "steps"
        }
    }
    
    
    var _startingAddress : String!
    var startingAddress : String {
        get {
            return _startingAddress
        }
        set {
            self._startingAddress = newValue
        }
    }
    
    
    var _endingAddress : String!
    var endingAddress : String {
        get {
            return _endingAddress
        }
        set {
            self._endingAddress = newValue
        }
    }
    
    
    var _steps : [Step]!
    var steps : [Step] {
        get {
            return _steps
        }
        set {
            self._steps = newValue
        }
    }
    
    
    //Constructor
    init (startingAddress : String, endingAddress : String) {
        
        self._startingAddress = startingAddress
        self._endingAddress = endingAddress
    }
    
} //class
