//
//  DetailViewController_UpcomingEvents.swift
//  mobileapplication
//
//  Created by Hope Benziger on 10/12/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import UIKit
import CoreLocation
class DetailViewController_UpcomingEvents: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var findLocationUIButton: UIButton!
    @IBOutlet weak var descUITextView: UITextView!
    @IBOutlet weak var addressUILabel: UILabel!
   // @IBOutlet weak var longDesUITextView: UITextView!
    @IBOutlet weak var eventUIImageView: UIImageView!
    @IBOutlet weak var descriptionUILabel: UILabel!
    @IBOutlet weak var dateTimeUILabel: UILabel!
  //  @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var endTImeUILabel: UILabel!
  // @IBOutlet weak var detailDescriptionLabel: UILabel!
    
    @IBOutlet weak var phoneUIButton: UIButton!
    @IBAction func callNumber(sender: AnyObject) {
        var phoneNumber : String = (detailEvent?.phone)!
        phoneNumber = phoneNumber.digitsOnly()
       
        if let phoneCallURL = NSURL(string: "tel:\(phoneNumber)") {
            let application = UIApplication.sharedApplication()
            if application.canOpenURL(phoneCallURL) {
                application.openURL(phoneCallURL)
            }
            else{
                print("failed")
            }
        }
        //if let url = NSURL(string: "tel://9193890567") {
         //  UIApplication.sharedApplication().openURL(url)
        //}
 //UIApplication.sharedApplication().openURL(NSURL(string: "telprompt://9193890567")!)
        
      //if let url = NSURL(string: "tel://\(detailEvent?.phone)") {
         //   UIApplication.sharedApplication().openURL(url)
      // }
    }
    var detailEvent: Event?
    let locationManager = CLLocationManager()
    var phoneLatitude : Double = 0;
    var phoneLontitude : Double = 0;
    
    @IBAction func findLocation(sender: AnyObject) {
        Map.openMapWithCoordinates((detailEvent?.department.longitude)!, destinationLat: (detailEvent?.department.latitude)!, sourceLon : phoneLontitude , sourceLat : phoneLatitude, destinationLocationName:(detailEvent?.name)!)
     
        
    }
    
    func configureEvent () {
      dateTimeUILabel.text = detailEvent?.formatedStartDate
        endTImeUILabel.text = detailEvent?.formatedEndDate
    //let requiredHeight = dateTimeUILabel.requiredHeight()
       // self.dateTimeUILabel.frame.size.height = requiredHeight
      //descriptionUILabel.text = detailEvent?.generalInformation
        descUITextView.text = detailEvent?.generalInformation
        addressUILabel.text = detailEvent?.location
      //  longDesUITextView.text  = detailEvent?.generalInformation
      self.title = detailEvent?.name
        
        let decodedData = NSData(base64EncodedString: (detailEvent?.department.icon)!, options: NSDataBase64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: decodedData!)
    
        let attachment = NSTextAttachment()
        attachment.image = decodedimage
       // phoneUIButton.te
        var phoneDisplay : String = (detailEvent?.phone)!
        phoneDisplay = "Call \(phoneDisplay)"
         phoneUIButton.setTitle(phoneDisplay, forState: .Normal)
        //put your NSTextAttachment into and attributedString
        
       eventUIImageView.image = decodedimage
       

        
    }
    func goBack(){
        //self.navigationController?.popToRootViewControllerAnimated(true)
      // navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
       print(" Error get phone location \(error) ")
    }
 
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        phoneLatitude = locValue.latitude
        phoneLontitude = locValue.longitude
        /*
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: phoneLatitude, longitude: phoneLontitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            print(placeMark.addressDictionary)
            
            // Location name
            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                print(locationName)
            }
            
            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
            }
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
            }
            
            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
            }
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                print(country)
            }
            
        })
        */
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    
    override func viewDidLoad() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        super.viewDidLoad()
        
   
        
        let navigationBar = navigationController!.navigationBar
        navigationBar.tintColor = UIColor.blueColor()
        
        let leftButton =  UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DetailViewController_UpcomingEvents.goBack))
        //let rightButton = UIBarButtonItem(title: "Right Button", style: UIBarButtonItemStyle.Plain, target: self, action: nil)
        
        navigationItem.leftBarButtonItem = leftButton
       // navigationItem.rightBarButtonItem = rightButton
        
        
       // self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "events.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "goBack")
        
        
       // self.navigationItem.leftItemsSupplementBackButton = true;
        // Do any additional setup after loading the view, typically from a nib.
        self.configureEvent()
    }
    
 
    
}
