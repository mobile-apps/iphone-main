//
//  LogUtils.swift
//  mobileapplication
//
//  Created by wtccuser on 12/29/15.
//  Copyright © 2015 wtccuser. All rights reserved.
//

import Foundation
/// <#Description#>

public class LogUtils : NSObject {
    let _mlog = XCGLogger.defaultInstance()
     public static let sharedInstance: LogUtils = LogUtils()
    /**
     <#Description#>
     
     - returns: <#return value description#>
     */
      private override init() {
        
        if let path = NSBundle.mainBundle().pathForResource("application", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                let loggingLevel : String = dict["loggingLevel"]!
                switch loggingLevel.lowercaseString
                {
                case "debug":
                    _mlog.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Debug)
                case "info":
                    _mlog.setup(.Info, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Info)
                case "warning":
                    _mlog.setup(.Warning, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Warning)
                case "error":
                    _mlog.setup(.Error, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Error)
                case "severe":
                    _mlog.setup(.Severe, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, fileLogLevel: .Severe)
                default:
                    _mlog.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true,  fileLogLevel: .Debug)
                }
                
                
            }
        }
        
        
        
        
     }
    
    
    public var Log : XCGLogger {
        get {
            
            return _mlog
        }
    }
   
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func ENTRY_LOG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) -> Void {
        
        sharedInstance.Log.debug("ENTRY", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func EXIT_LOG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) -> Void {
       sharedInstance.Log.debug("EXIT", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func DEBUG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        sharedInstance.Log.debug(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func INFO(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        sharedInstance.Log.info(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func WARNING(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        sharedInstance.Log.warning(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    /**
     Wrapper logs
     
     - parameter functionName: function name
     - parameter fileName:     file name to record log
     - parameter lineNumber:   source line number
     */
    public class func SEVERE(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__,displayText:String) -> Void {
        sharedInstance.Log.severe(displayText, functionName:functionName, fileName:fileName, lineNumber:lineNumber)
    }
    
}

