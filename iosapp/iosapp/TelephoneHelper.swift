//
//  TelephoneHelper.swift
//  iosapp
//
//  Created by wtccuser on 5/6/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import Foundation
import UIKit
public class TelephoneHelper : NSObject {
    class public func callNumber(phoneNumber:String) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
}
