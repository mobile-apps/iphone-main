//
//  Category.swift
//  mobileapplication
//
//  Created by Christine LaBeff on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation


typealias id = String
typealias name = String
/// <#Description#>
public class Category {
    
    // MARK: define json column names used to parse json object
    /// <#Description#>
    class var IDProperty : String {
        get {
            return "id"
        }
    }
    /// <#Description#>
    class var NameProperty : String {
        get {
            return "name"
        }
    }
    
    
    // MARK: Getters and Setters
    var _ID :String!
    var ID : String {
        get {
            return _ID
        }
        set {
            self._ID = newValue
        }
    }
    
    var _name :String!
    /// <#Description#>
    var Name : String {
        get {
            return _name
        }
        set {
            self._name = newValue
        }
    }
    
    
    /**
    <#Description#>
    
    - parameter ID:   <#ID description#>
    - parameter Name: <#Name description#>
    
    - returns: <#return value description#>
    */
    init(ID: id, Name: name) {
        self._ID = ID
        self._name = Name
    }
    
}