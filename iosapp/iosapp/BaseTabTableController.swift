//
//  BaseTabTableController.swift
//  iosapp
//
//  Created by wtccuser on 4/25/16.
//  Copyright © 2016 wtccuser. All rights reserved.
//

import UIKit
class BaseTabTableController: UITableViewController {
    //var leftTabIndex = 0
    
    
    
    var _leftTabIndex : Int = 2
    var leftTabIndex : Int {
        get {
            return _leftTabIndex;
        }
        set {
            self._leftTabIndex = newValue
        }
    }
    var _rightTabIndex = 0
    var rightTabIndex : Int {
        get {
            return _rightTabIndex;
        }
        set {
            self._rightTabIndex = newValue
        }
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    override func shouldAutorotate() -> Bool {
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tbc = storyboard.instantiateViewControllerWithIdentifier("tabbarcontrolId") as! UITabBarController
        self.presentViewController(tbc, animated: true, completion: nil)
        if (sender.direction == .Left) {
            
            tbc.selectedIndex=leftTabIndex;
            print("Swipe Left")
            
        }
        
        if (sender.direction == .Right) {
            print("Swipe Right")
            tbc.selectedIndex=rightTabIndex;
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        LogUtils.INFO(displayText: "starting viewDidLoad")
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(BaseTabUIViewController.handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(BaseTabUIViewController.handleSwipes(_:)))
        
        leftSwipe.direction = .Left
        rightSwipe.direction = .Right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
    }
    
}
