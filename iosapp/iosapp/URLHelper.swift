//
//  URLHelper.swift
//  mobileapplication
//
//  Created by wtccuser on 8/29/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.

/**
<#Description#> steps to used the files
1. Update the urls.plist file with the appropriate
url address and key value
2. Add a new enumeration to the enum URLAddress. The name should be the same as the key define in step 1
3. Add a case statement to the switch statement in the function getURLAddress
*/
import Foundation
/**
This is a list of all of the remote json web services

- GetAllDepartments: The sub URL address used to get a list of all departments
- GetAllPodcasts:    The sub URL address used to get a list of all podcasts
- GetDirections:     <#GetDirections description#>
*/
public enum URLAddress
{
    case GetAllDepartments
    case GetAllPodcasts
    case GetDirections
    case GetCrashReport
    case GetAllEvents
    
    
}
/// This class is used to get the JSON url address from the urls.plist in the supporting files directory
public class URLHelper : NSObject {
    /**
     * Get the url address of the web service based on the urlAddress enumeration
    
    - parameter urlAddress: enumeration for the Url addresses
    
    - returns: url address for the json remote web service
    */
    public class func getURLAddress(urlAddress: URLAddress) -> String {
        ///this is the root url domain. This is added to every JSON url
        LogUtils.ENTRY_LOG()
        var rootURL : String = String()
        
        //get the root url address
        //load the property file into a dictionary
        if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                rootURL  = dict["rootURL"]!
            }
        }//end of if let path
        
        ///the variable used to store the return address
        var returnURLAddres : String = String()//initialize to empty string
        /**
        *  <#Description#> perform the switch statement
        * to compute the url address
        */
        switch urlAddress
        {
        case URLAddress.GetAllDepartments:
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    returnURLAddres  = dict["url_getAllDepartments"]!
                    returnURLAddres = rootURL + returnURLAddres
                }
            }//end of if let path
        
        case URLAddress.GetCrashReport:
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    returnURLAddres  = dict["url_createcrash"]!
                    returnURLAddres = rootURL + returnURLAddres
                }
            }//
        case URLAddress.GetDirections:
            
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    
                    returnURLAddres  = dict["url_directions"]!
                    returnURLAddres = rootURL + returnURLAddres
                    
                } //if let dict
                
            }//if let path
            
        case URLAddress.GetAllPodcasts:
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    returnURLAddres  = dict["url_getAllPodcasts"]!
                    returnURLAddres = rootURL + returnURLAddres as String
                }
            }//end of if let path
        case URLAddress.GetAllEvents:
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    returnURLAddres  = dict["url_getAllEvents"]!
                    returnURLAddres = rootURL + returnURLAddres as String
                }
            }//end of if let path
            
       // default:
          //  returnURLAddres = String()
            
        }//end of switch
        LogUtils.EXIT_LOG()
        return returnURLAddres
        //URLHelper.getURLAddress(URLAddress.GetAllDepartments)
        
    }//end of func
    
    
}//end of class
